(ns clojure.core.async.extension
  (:require [clojure.core.async :as async]
            [clojure.core.async.impl.channels :as async-ch]
            [clojure.core.async.impl.protocols :as async-proto])
  (:import java.util.concurrent.locks.Lock))


(defn <!!
  "TODO

  Example:
   (<!! (`async/chan`) 1000)"
  ([ch]
   (async/<!! ch))
  ([ch timeout]
   (<!! ch timeout :timed-out))
  ([ch timeout timeout-val]
   (let [p (promise)
         timed-out? (atom false)
         handler (reify
                   Lock
                   (lock [_])
                   (unlock [_])

                   async-proto/Handler
                   (active? [_] (not @timed-out?))
                   (blockable? [_] true)
                   (lock-id [_] 0)
                   (commit [_] #(deliver p %)))
         ret (async-proto/take! ch handler)]
     (if ret
       @ret
       (let [v (deref p timeout timeout-val)]
         (when (= timeout-val v)
           (reset! timed-out? true)
           (async-ch/cleanup ch))
         v)))))


(defn closed?
  "If an async channel was closed, return true otherwise return false.
  For chained async channels, wait for up to 1 second to close.

  Example:
   (closed-ch? #obj[...Channel])"
  [ch]
  (if (async-proto/closed? ch)
    true
    (nil? (<!! ch 1000))))
