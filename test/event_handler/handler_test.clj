(ns event-handler.handler-test
  (:require [clojure.core.async :as async]
            [clojure.core.async.extension :as async-ext]
            [clojure.test :refer :all]
            [dev.test :as test]
            [event-handler.handler :as target]
            [spy.core :as spy])
  (:import java.time.Instant
           java.time.temporal.ChronoUnit))

(use-fixtures :once test/without-trivial-log)


(deftest examples
  (testing "resolve-vars"
    (is      (= "key is value" (target/resolve-vars {"key" "value"} "key is %{key}")))
    (is (not (= "key is value" (target/resolve-vars {:key  "value"} "key is %{key}")))
        "variable table's keys should be the string."))

  (testing "handler-fn"
    (is (fn? (target/handler-fn {:name "test" :exec ["/bin/bash" "-c" "..."]})))
    (is (= 0 (-> ((target/handler-fn {:name "test" :exec ["/bin/bash" "-c" "true"]})  nil) (first) (async-ext/<!! 1000))))
    (is (= 1 (-> ((target/handler-fn {:name "test" :exec ["/bin/bash" "-c" "false"]}) nil) (first) (async-ext/<!! 1000)))))

  (testing "run-handler"
    (is (= :ok (let [f (fn [& _]
                         (let [ch (async/promise-chan)]
                           (async/>!! ch :ok)
                           [ch nil]))
                     ch (target/run-handler {:name "test" :func f})]
                 (async-ext/<!! ch 1000)))))

  (testing "start-timer"
    ;; TODO
    )

  (testing "start"
    (is (= :ok (let [p (promise)
                     f (fn [& _] (deliver p :ok) p)
                     {:keys [ch] :as conn} (target/start [{:name "test" :func f}])]
                 (async/>!! ch (target/event "test" nil))
                 (target/stop conn)
                 @p))))

  (testing "stop"
    ;; TODO
    ;;  how to test?
    (is (nil? (let [{:keys [ch] :as conn} (target/start nil)]
                (target/stop conn)
                (async/<!! ch)))))

  (testing "event"
    (let [name "change-cpu-freq-policy"
          var-tbl {"key" "value"}
          delay 10]
      (is (= {:handler-name name
              :var-tbl var-tbl
              :delay nil}
             (target/event name var-tbl)))

      (is (= {:handler-name name
              :var-tbl var-tbl
              :delay delay}
             (target/event name var-tbl delay))))))


(deftest test-for-delayed-handler
  (testing "add a handler to `handler-timer`"
    (let [timer-ch (async/chan)
          start-timer (spy/stub timer-ch)
          handler {:name "test" :func (fn [& _] (doto (promise) (deliver 0)))}
          handler-ch (async/chan)
          var-tbl {:a "a"}
          delay 1000
          now (Instant/now)]
      (with-redefs [target/start-delay-executor start-timer]
        (target/start [handler] handler-ch))

      (async/put! handler-ch (target/event "test" var-tbl delay))
      (let [x (async-ext/<!! timer-ch 1000 nil)]
        (is (= handler (:handler x)))
        (is (= var-tbl (:var-tbl x)))
        (is (not (= (:time x) (.. now (plusMillis delay) (truncatedTo ChronoUnit/NANOS))))))))

  (testing "run a delayed handler"
    ;; TODO
    ;;  how to test?
    ))
