(ns event-handler.dbus.marshalling-test
  (:require [clojure.test :refer :all]
            [dev.test :as test]
            [event-handler.dbus.marshalling :as target]))

(use-fixtures :once test/without-trivial-log)


(deftest examples
  (testing "seq-sig"
    (is (= "x" (target/seq-sig [1 2 3])))
    (is (= "v" (target/seq-sig [1 1.5 2]))))

  (testing "type-sig"
    (is (= "b"  (target/type-sig java.lang.Boolean)))
    (is (= "ai" (target/type-sig (type (int-array []))))))

  (testing "sig"
    (is (= "x"   (target/sig 0)))
    (is (= "xsd" (target/sig 0 "1" 3.0)))
    (is (= "av"  (target/sig [0 "1"])))
    (is (= "ax"  (target/sig [0 1]))))

  (testing "dbus-vals"
    ;; TODO
    )

  (testing "flag"
    (is (instance? Byte (target/flag)))
    (is (= 0x00 (target/flag)))
    (is (= 0x02 (target/flag :auto-start? true)))
    (is (= 0x07
           (target/flag {:no-reply?         true
                         :auto-start?       true
                         :interactive-auth? true})))))


(deftest generate-signature
  (testing "vector"
    (is (= "as" (target/sig ["1" "2" "3"])))
    (is (= "av" (target/sig ["1"  2  "3"]))))

  (testing "list"
    (is (= "as" (target/sig '("1" "2" "3"))))
    (is (= "av" (target/sig '("1"  2  "3")))))

  (testing "lazy-seq"
    (is (= "as" (target/sig (map identity ["1" "2" "3"]))))
    (is (= "av" (target/sig (map identity ["1"  2  "3"])))))

  (testing "set"
    (is (= "as" (target/sig #{"1" "2" "3"})))
    (is (= "av" (target/sig #{"1"  2  "3"}))))

  (testing "map"
    (is (= "a{sv}" (target/sig {:a 1 :b  2  :c 3})))
    (is (= "a{sv}" (target/sig {:a 1 :b "2" :c 3})))))


#_(deftest marshalling-clojure-data-structure
    (testing "vector"
      ;; TODO
      )

    (testing "list"
      ;; TODO
      )

    (testing "lazy-seq"
      ;; TODO
      )

    (testing "set"
      ;; TODO
      )

    (testing "map"
      ;; TODO
      ))

