(ns event-handler.dbus.internal.utils-test
  (:require [clojure.test :refer :all]
            [event-handler.dbus.internal.utils :as target]))


(deftest examples
  (testing "glob->regex"
    (is (= "^/a/b/[^/]*$"   (str (target/glob->regex "/a/b/*"))))
    (is (= "^/a/b/.*$"      (str (target/glob->regex "/a/b/**"))))
    (is (= "^/a/b/[^/]?$"   (str (target/glob->regex "/a/b/?"))))
    (is (= "^/a/b/[abc]$"   (str (target/glob->regex "/a/b/[abc]"))))
    (is (= "^/a/b/[a-c]$"   (str (target/glob->regex "/a/b/[a-c]"))))
    (is (= "^/a/b/(a|b|c)$" (str (target/glob->regex "/a/b/{a,b,c}"))))
    (is (= "^/a/b/\\Q*\\E$" (str (target/glob->regex "/a/b/\\*")))))

  (testing "matched-sig-idxes-lst"
    (is (= [[0] [2]] (target/sig->idxes-lst {:sig "i"}                  ["i" "b" "i"])))
    (is (= [[0 1]]   (target/sig->idxes-lst {:sig "i" :next {:sig "b"}} ["i" "b" "i"])))))


(deftest convert-glob-pattern-to-regular-expression
  (testing "No glob patterns"
    (let [regex (target/glob->regex "/a/b/c")]
      (is      (re-find regex "/a/b/c"))
      (is (not (re-find regex "/a/b/c/d")))))

  (testing "'*' and '**' glob pattern"
    (let [regex (target/glob->regex "/a/b/\\*")]
      (is      (re-find regex "/a/b/*"))
      (is (not (re-find regex "/a/b/..."))))

    (let [regex (target/glob->regex "/a/b/*")]
      (is (not (re-find regex "/a/B/")))
      (is      (re-find regex "/a/b/"))
      (is      (re-find regex "/a/b/c"))
      (is      (re-find regex "/a/b/c..."))
      (is (not (re-find regex "/a/b/c/d"))))

    (let [regex (target/glob->regex "/a/b/**")]
      (is (not (re-find regex "/a/B/")))
      (is      (re-find regex "/a/b/"))
      (is      (re-find regex "/a/b/c"))
      (is      (re-find regex "/a/b/c..."))
      (is      (re-find regex "/a/b/c/d"))
      (is      (re-find regex "/a/b/c/d/..."))))

  (testing "'?' glob pattern"
    (let [regex (target/glob->regex "/a/b/a?")]
      (is      (re-find regex "/a/b/a"))
      (is      (re-find regex "/a/b/a."))
      (is      (re-find regex "/a/b/a9"))
      (is      (re-find regex "/a/b/az"))
      (is (not (re-find regex "/a/b/abc")))))

  (testing "'[...]' glob pattern"
    (let [regex (target/glob->regex "/a/b/a[b-c]")]
      (is (not (re-find regex "/a/b/a")))
      (is      (re-find regex "/a/b/ab"))
      (is      (re-find regex "/a/b/ac"))
      (is (not (re-find regex "/a/b/ad")))
      (is (not (re-find regex "/a/b/abc")))))

  (testing "'{...}' glob pattern"
    (let [regex (target/glob->regex "/a/b/{a,b,c}")]
      (is (not (re-find regex "/a/b/")))
      (is      (re-find regex "/a/b/a"))
      (is      (re-find regex "/a/b/b"))
      (is      (re-find regex "/a/b/c"))
      (is (not (re-find regex "/a/b/d")))))

  (testing "escape"
    (let [regex (target/glob->regex "/a/b/\\*")]
      (is      (re-find regex "/a/b/*"))
      (is (not (re-find regex "/a/b/..."))))
    (let [regex (target/glob->regex "/a/b/a\\?")]
      (is      (re-find regex "/a/b/a?"))
      (is (not (re-find regex "/a/b/a."))))
    (let [regex (target/glob->regex "/a/b/a\\[b-c\\]")]
      (is      (re-find regex "/a/b/a[b-c]"))
      (is (not (re-find regex "/a/b/ab")))
      (is (not (re-find regex "/a/b/ac"))))
    (let [regex (target/glob->regex "/a/b/\\{a,b,c\\}")]
      (is      (re-find regex "/a/b/{a,b,c}"))
      (is (not (re-find regex "/a/b/a")))
      (is (not (re-find regex "/a/b/b")))
      (is (not (re-find regex "/a/b/c"))))
    (let [regex (target/glob->regex "/a/b/\\\\")]
      (is (re-find regex "/a/b/\\")))))


(deftest convert-signature-to-indexes-list
  (testing "single"
    (is (= [[0] [2] [3] [6] [8]] (target/sig->idxes-lst {:sig "i"} ["i" "ax" "i" "i" "s" "s" "i" "s" "i" "s"]))))

  (testing "chained"
    (is (= [[3 4] [6 7] [8 9]] (target/sig->idxes-lst {:sig "i" :next {:sig "s"}}                  ["i" "ax" "i" "i" "s" "s" "i" "s" "i" "s"])))
    (is (= [[4 5 6]]           (target/sig->idxes-lst {:sig "i" :next {:sig "s" :next {:sig "b"}}} ["i" "ax" "i" "s" "i" "s" "b" "s" "i" "s"])))))
