(ns event-handler.dbus.msg-test
  (:require [clojure.test :refer :all]
            [dev.test :as test]
            [event-handler.dbus.msg :as target]))

(use-fixtures :once test/without-trivial-log)


(deftest examples
  (testing "method-call"
    (let [path "/org/freedesktop/DBus"
          inf "org.freedesktop.DBus"
          memb "AddMatch"
          msg (target/method-call path inf memb ["Type='signal'"])]
      (is (= path (.getPath msg)))
      (is (= inf  (.getInterface msg)))
      (is (= memb (.getName msg)))
      ;; TODO
      ;;  check parameters
      ))

  (testing "signal"
    (let [path "/org/freedesktop/login1/session/_32"
          inf "org.freedesktop.DBus.Properties"
          memb "PropertiesChanged"
          msg (target/signal path inf memb ["org.freedesktop.login1.Session" {"LockedHint" true}])]
      (is (= path (.getPath msg)))
      (is (= inf  (.getInterface msg)))
      (is (= memb (.getName msg)))
      ;; TODO
      ;;  check parameters
      ))

  (testing "msg-type"
    (let [msg-a (target/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])
          msg-b (target/signal "/org/freedesktop/login1/session/_32"
                               "org.freedesktop.DBus.Properties"
                               "PropertiesChanged"
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])]
      (is (= target/method-call-type (target/msg-type msg-a)))
      (is (= target/signal-type      (target/msg-type msg-b)))))

  (testing "msg-type->kw"
    (is (= :method-call   (target/msg-type->kw target/method-call-type)))
    (is (= :method-return (target/msg-type->kw target/method-return-type)))
    (is (= :error         (target/msg-type->kw target/error-type)))
    (is (= :signal        (target/msg-type->kw target/signal-type))))

  (testing "kw->msg-type"
    (is (= target/method-call-type   (target/kw->msg-type :method-call)))
    (is (= target/method-return-type (target/kw->msg-type :method-return)))
    (is (= target/error-type         (target/kw->msg-type :error)))
    (is (= target/signal-type        (target/kw->msg-type :signal))))

  (testing "msg-type-check-fn"
    (let [msg-a (target/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])
          msg-b (target/signal "/org/freedesktop/login1/session/_32"
                               "org.freedesktop.DBus.Properties"
                               "PropertiesChanged"
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])
          signal-msg? (target/msg-type-check-fn target/signal-type)]
      (is (not (signal-msg? msg-a)))
      (is      (signal-msg? msg-b))))

  (testing "msg-path"
    (let [path-a "/org/freedesktop/DBus"
          path-b "/org/freedesktop/login1/session/_32"
          msg-a (target/method-call path-a "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])
          msg-b (target/signal path-b
                               "org.freedesktop.DBus.Properties"
                               "PropertiesChanged"
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])]
      (is (= path-a (target/msg-path msg-a)))
      (is (= path-b (target/msg-path msg-b)))))

  (testing "msg-path"
    (let [path "/org/freedesktop/login1/session/_32"
          msg-a (target/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])
          msg-b (target/signal path
                               "org.freedesktop.DBus.Properties"
                               "PropertiesChanged"
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])
          login-sess-path? (target/msg-path-check-fn path)]
      (is (not (login-sess-path? msg-a)))
      (is      (login-sess-path? msg-b))))

  (testing "msg-type-check-fn"
    (let [msg-a (target/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])
          msg-b (target/signal "/org/freedesktop/login1/session/_32"
                               "org.freedesktop.DBus.Properties"
                               "PropertiesChanged"
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])
          signal-msg? (target/msg-type-check-fn target/signal-type)]
      (is (not (signal-msg? msg-a)))
      (is      (signal-msg? msg-b))))

  (testing "msg-interface"
    (let [inf-a "org.freedesktop.DBus"
          inf-b "org.freedesktop.DBus.Properties"
          msg-a (target/method-call "/org/freedesktop/DBus" inf-a "AddMatch" ["Type='signal'"])
          msg-b (target/signal "/org/freedesktop/login1/session/_32"
                               inf-b
                               "PropertiesChanged"
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])]
      (is (= inf-a (target/msg-interface msg-a)))
      (is (= inf-b (target/msg-interface msg-b)))))

  (testing "msg-interface-check-fn"
    (let [inf "org.freedesktop.DBus.Properties"
          msg-a (target/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])
          msg-b (target/signal "/org/freedesktop/login1/session/_32"
                               inf
                               "PropertiesChanged"
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])
          prop-msg? (target/msg-interface-check-fn inf)]
      (is (not (prop-msg? msg-a)))
      (is      (prop-msg? msg-b))))

  (testing "msg-member"
    (let [memb-a "AddMatch"
          memb-b "PropertiesChanged"
          msg-a (target/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" memb-a ["Type='signal'"])
          msg-b (target/signal "/org/freedesktop/login1/session/_32"
                               "org.freedesktop.DBus.Properties"
                               memb-b
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])]
      (is (= memb-a (target/msg-member msg-a)))
      (is (= memb-b (target/msg-member msg-b)))))

  (testing "msg-member-check-fn"
    (let [memb "PropertiesChanged"
          msg-a (target/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])
          msg-b (target/signal "/org/freedesktop/login1/session/_32"
                               "org.freedesktop.DBus.Properties"
                               memb
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])
          prop-changed-msg? (target/msg-member-check-fn memb)]
      (is (not (prop-changed-msg? msg-a)))
      (is      (prop-changed-msg? msg-b))))

  (testing "msg-vals-sig"
    (let [msg-a (target/method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])
          msg-b (target/signal "/org/freedesktop/login1/session/_32"
                               "org.freedesktop.DBus.Properties"
                               "PropertiesChanged"
                               ["org.freedesktop.login1.Session" {"LockedHint" true}])]
      (is (= "s"      (target/msg-vals-sig msg-a)))
      (is (= "sa{sv}" (target/msg-vals-sig msg-b)))))

  (testing "msg-vals"
    ;; TODO
    )

  (testing "vars-extraction-fn"
    (let [msg (target/signal "/org/freedesktop/login1/session/_32"
                             "org.freedesktop.DBus.Properties"
                             "PropertiesChanged"
                             ["org.freedesktop.login1.Session" {"LockedHint" true}])
          get-var-tbl (target/vars-extraction-fn
                       [{:type :str
                         :val "org.freedesktop.login1.Session"}
                        {:type :dict
                         :val [{:type :bool
                                :name "LockedHint"
                                :val "%{any}"
                                :var "is-lock"}]
                         :pos "%{continuous}"}])]
      (is (= {"is-lock" true} (get-var-tbl msg))))))


(deftest get-variable-table-from-dbus-msg
  (testing "with a bit complicated specification"
    (let [msg (target/signal "path" "interface" "member"
                             ["arg: 3"
                              "arg: 1"
                              "arg: 4"
                              "arg: 2"])
          get-var-tbl (target/vars-extraction-fn
                       [{:type :str, :val "arg: 4" :var "4"}
                        {:type :str, :val "arg: 1" :var "1"}
                        {:type :str, :val "arg: 3" :var "3"}
                        {:type :str, :val "arg: 2" :var "2"}])]
      (is (= {"4" "arg: 4"
              "3" "arg: 3"
              "2" "arg: 2"
              "1" "arg: 1"}
             (get-var-tbl msg)))))

  (testing "with more complicated specification"
    (let [msg-a (target/signal "path" "interface" "member" ["a" "a" {"key" "value"}])
          msg-b (target/signal "path" "interface" "member" ["a" {"key" "value"} "a"])
          get-var-tbl (target/vars-extraction-fn
                       [{:type :str, :val "a" :var "A"}
                        {:type :str, :val "a"}
                        {:type :dict
                         :val [{:type :str
                                :name "key"
                                :val "%{any}"
                                :var "v"}]
                         :pos "%{continuous}"}])]
      (is (= {"A" "a", "v" "value"} (get-var-tbl msg-a)))
      (is (= {"A" "a", "v" "value"} (get-var-tbl msg-b))))))
