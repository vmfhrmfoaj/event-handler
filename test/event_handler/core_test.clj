(ns event-handler.core-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as spec]
            [clojure.spec.gen.alpha :as gen]
            [event-handler.core :as target]
            [event-handler.core-spec :as target-spec]
            [event-handler.core-gen]
            [spy.core :as spy]))


(deftest spy-example
  (testing "how to use spy"
    (is (let [x (spy/stub nil)]
          (x 1 2 3 4)
          (spy/called-with? x 1 2 3 4)))))


(deftest test-check-example
  (testing "how to get test data form the specification"
    (is (get (->> "simple"
                  (map str)
                  (into #{}))
             (gen/generate (spec/gen ::target-spec/simple))))))
