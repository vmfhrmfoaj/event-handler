(ns event-handler.time-test
  (:require [clojure.test :refer :all]
            [event-handler.time :as target])
  (:import (java.time Instant LocalDateTime ZoneOffset)
           (java.time.temporal ChronoField ChronoUnit)))


(deftest examples
  (let [t1 (Instant/ofEpochSecond 1682976450)  ; date -d "2023-05-01 21:27:30Z" +%s
        t2 (Instant/ofEpochSecond 1682976731)] ; date -d "2023-05-01 21:32:11Z" +%s # +281 seconds
    (testing "epoch"
      (is (= (Instant/EPOCH) target/epoch)))

    (testing "now"
      (is (instance? Instant (target/now))))

    (testing "local-time"
      (is (instance? LocalDateTime (target/local-time t1)))
      (is (instance? LocalDateTime (target/local-time t2 ZoneOffset/UTC))))

    (testing "year/month/day/hour/minute/second"
      (is (= 2023   (target/year   t1)))
      (is (=   05   (target/month  t1)))
      (is (=   01   (target/day    t1)))
      (is (=   21   (target/hour   t1)))
      (is (=   27   (target/minute t1)))
      (is (=   30.0 (target/second t1)))
      (is (=   30          (target/second t1 :sec)))
      (is (=   30000       (target/second t1 :milli)))
      (is (=   30001       (target/second (.plus t1 1 ChronoUnit/MILLIS) :milli)))
      (is (=   30000000    (target/second t1 :micro)))
      (is (=   30000001    (target/second (.plus t1 1 ChronoUnit/MICROS) :micro)))
      (is (=   30000000000 (target/second t1 :nano)))
      (is (=   30000000001 (target/second (.plus t1 1 ChronoUnit/NANOS)  :nano))))

    (testing "time"
      (is (= target/epoch (target/time :year 1970 :month 01 :day 01 :hour 00 :minute 00 :second 00)))
      (is (with-redefs [target/now (constantly t1)] (= t1 (target/time))))
      (is (= t1 (target/time :base-time t1)))
      (is (= 1970   (target/year   (target/time :base-time t1 :year   1970))))
      (is (=   01   (target/month  (target/time :base-time t1 :month    01))))
      (is (=   01   (target/day    (target/time :base-time t1 :day      01))))
      (is (=   00   (target/hour   (target/time :base-time t1 :hour     00))))
      (is (=   00   (target/minute (target/time :base-time t1 :minute   00))))
      (is (=    0.0 (target/second (target/time :base-time t1 :second   00)))))

    (testing "between"
      (is (=  281.0 (target/second (target/between t1 t2))))
      (is (= -281.0 (target/second (target/between t2 t1))))
      (is (=  281   (target/second (target/between t1 t2) :sec)))
      (is (=    1   (target/second (target/between t1 (.plus t1 1 ChronoUnit/MILLIS)) :milli)))
      (is (=    1   (target/second (target/between t1 (.plus t1 1 ChronoUnit/MICROS)) :micro)))
      (is (=    1   (target/second (target/between t1 (.plus t1 1 ChronoUnit/NANOS)) :nano))))

    (testing "plus"
      (is (= t2 (target/plus t1  281 :second)))
      (is (= t1 (target/plus t2 -281 :second)))
      (is (= t2 (target/plus t1 (target/between t1 t2))))
      (is (= t1 (target/plus t2 (target/between t2 t1))))
      (is (= t2 (-> t1 (target/plus 280.0 :second) (target/plus 0.5 :second) (target/plus 0.5 :second)))))

    (testing "minus"
      (is (= t1 (target/minus t2  281 :second)))
      (is (= t2 (target/minus t1 -281 :second)))
      (is (= t1 (target/minus t2 (target/between t1 t2))))
      (is (= t2 (target/minus t1 (target/between t2 t1))))
      (is (= t1 (-> t2 (target/minus 280.0 :second) (target/minus 0.5 :second) (target/minus 0.5 :second)))))

    (testing "truncate"
      (is (zero?      (.get (target/truncate t1 :second) ChronoField/MILLI_OF_SECOND)))
      (is (zero? (mod (.get (target/truncate t1 :milli)  ChronoField/MICRO_OF_SECOND) 1000)))
      (is (zero? (mod (.get (target/truncate t1 :micro)  ChronoField/NANO_OF_SECOND)  1000))))

    (testing "before?"
      (is      (target/before? t1 t2))
      (is (not (target/before? t2 t1))))

    (testing "after?"
      (is (not (target/after? t1 t2)))
      (is      (target/after? t2 t1)))))
