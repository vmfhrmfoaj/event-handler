(ns event-handler.config-test
  (:require [clojure.string :as str]
            [clojure.test :refer :all]
            [event-handler.config :as target])
  (:import clojure.lang.ExceptionInfo))


(deftest examples
  (testing "parse-string"
    (is (= {:log {:file "/tmp/event-handler.log" :level :info}
            :handlers [{:name "screen-lock-unlock"
                        :delay 10
                        :retry-after nil
                        :exec ["/bin/bash" "-c" "echo $(if [ 'true' = %{is-lock} ]; then echo 'locked'; else echo 'unlocked'; fi)"]}
                       {:name "wake-up"
                        :delay 10
                        :retry-after nil
                        :exec ["/bin/echo" "program should be absolute path" "and it should be exists"]}
                       {:name "wake-up-2"
                        :delay 1000
                        :retry-after 500
                        :exec ["/bin/echo" "this is secondary action for wake-up trigger (dbus.path = %{dbus.path})"]}]
            :triggers [{:name       "Screen (un)lock"
                        :interval   1000
                        :conj       :or
                        :time-slice 500
                        :seq-check? false
                        :quit-on-error? true
                        :sources [{:type :dbus
                                   :data {:bus       :system
                                          :type      :signal
                                          :path      "/org/freedesktop/login1/session/**"
                                          :interface "org.freedesktop.DBus.Properties"
                                          :member    "PropertiesChanged"
                                          :args [{:type :str
                                                  :val  "org.freedesktop.login1.Session"}
                                                 {:type :dict
                                                  :pos  "%{continuous}"
                                                  :val  [{:type :bool
                                                          :name "LockedHint"
                                                          :val "%{any}"
                                                          :var "is-lock"}]}]}}]
                        :handlers [{:name "screen-lock-unlock"}]}
                       {:name       "Wake up"
                        :interval   200
                        :conj       :or
                        :time-slice 500
                        :seq-check? false
                        :quit-on-error? true
                        :sources [{:type :dbus
                                   :data {:bus       :system
                                          :type      :signal
                                          :path      "/org/freedesktop/login1/*"
                                          :interface "org.freedesktop.login1.Manager"
                                          :member    "PrepareForSleep"
                                          :args [{:type :bool
                                                  :val  false}]}}]
                        :handlers [{:name "wake-up" :delay 3000}
                                   {:name "wake-up-2"}]}]}
           (target/load-conf-file "dev-resources/config.example.toml")))))


(defn- result
  [& lines]
  (re-pattern (str "\\Q" (str/join "\n" lines) "\\E")))


(deftest check-invalid-config
  (testing "log"
    (let [handler
          (str/join "\n"
                    ["[[handler]]"
                     "name = \"test\""
                     "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"])

          trigger
          (str/join "\n"
                    ["[[trigger]]"
                     "name = \"test\""
                     "[[trigger.dbus]]"
                     "bus = \"system\""
                     "type = \"signal\""
                     "path = \"/org/freedesktop/login1/session/**\""
                     "interface = \"org.freedesktop.DBus.Properties\""
                     "member = \"PropertiesChanged\""
                     "[[trigger.handler]]"
                     "name = \"test\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [log]"
                   "2: file = \"not-exists-file\""
                   "   ^^^^^^^^^^^^^^^^^^^^^^^^"
                   "   Parent directory of it should be exists")
           (target/parse-string (str/join "\n"
                                          ["[log]"
                                           "file = \"not-exists-file\""
                                           handler
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [log]"
                   "2: level = \"trace\""
                   "   ^^^^^^^^^^^^^^^"
                   "   It should be one of \"debug\", \"info\", \"warn\" or \"error\"")
           (target/parse-string (str/join "\n"
                                          ["[log]"
                                           "level = \"trace\""
                                           handler
                                           trigger]))))))

  (testing "global.handler"
    (let [handler
          (str/join "\n"
                    ["[[handler]]"
                     "name = \"test\""
                     "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"])

          trigger
          (str/join "\n"
                    ["[[trigger]]"
                     "name = \"test\""
                     "[[trigger.dbus]]"
                     "bus = \"system\""
                     "type = \"signal\""
                     "path = \"/org/freedesktop/login1/session/**\""
                     "interface = \"org.freedesktop.DBus.Properties\""
                     "member = \"PropertiesChanged\""
                     "[[trigger.handler]]"
                     "name = \"test\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [global]"
                   "2: [global.handler]"
                   "3: delay = -1"
                   "   ^^^^^^^^^^"
                   "   It should be in between 0 and 3_600_000")
           (target/parse-string (str/join "\n"
                                          ["[global]"
                                           "[global.handler]"
                                           "delay = -1"
                                           handler
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [global]"
                   "2: [global.handler]"
                   "3: delay = 3_600_001"
                   "   ^^^^^^^^^^^^^^^^^"
                   "   It should be in between 0 and 3_600_000")
           (target/parse-string (str/join "\n"
                                          ["[global]"
                                           "[global.handler]"
                                           "delay = 3_600_001"
                                           handler
                                           trigger]))))))

  (testing "global.trigger"
    (let [handler (str/join "\n"
                            ["[[handler]]"
                             "name = \"test\""
                             "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"])
          trigger (str/join "\n"
                            ["[[trigger]]"
                             "name = \"test\""
                             "[[trigger.dbus]]"
                             "bus = \"system\""
                             "type = \"signal\""
                             "path = \"/org/freedesktop/login1/session/**\""
                             "interface = \"org.freedesktop.DBus.Properties\""
                             "member = \"PropertiesChanged\""
                             "[[trigger.handler]]"
                             "name = \"test\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [global]"
                   "2: [global.trigger]"
                   "3: interval = 4"
                   "   ^^^^^^^^^^^^"
                   "   It should be in between 5 and 1_000")
           (target/parse-string (str/join "\n"
                                          ["[global]"
                                           "[global.trigger]"
                                           "interval = 4"
                                           handler
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [global]"
                   "2: [global.trigger]"
                   "3: interval = 1001"
                   "   ^^^^^^^^^^^^^^^"
                   "   It should be in between 5 and 1_000")
           (target/parse-string (str/join "\n"
                                          ["[global]"
                                           "[global.trigger]"
                                           "interval = 1001"]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [global]"
                   "2: [global.trigger]"
                   "3: conjunction = \"xor\""
                   "   ^^^^^^^^^^^^^^^^^^^"
                   "   It should be \"or\" or \"and\"")
           (target/parse-string (str/join "\n"
                                          ["[global]"
                                           "[global.trigger]"
                                           "conjunction = \"xor\""]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [global]"
                   "2: [global.trigger]"
                   "3: time_slice = 99"
                   "   ^^^^^^^^^^^^^^^"
                   "   It should be in between 100 and 60_000")
           (target/parse-string (str/join "\n"
                                          ["[global]"
                                           "[global.trigger]"
                                           "time_slice = 99"
                                           handler
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [global]"
                   "2: [global.trigger]"
                   "3: time_slice = 60_001"
                   "   ^^^^^^^^^^^^^^^^^^^"
                   "   It should be in between 100 and 60_000")
           (target/parse-string (str/join "\n"
                                          ["[global]"
                                           "[global.trigger]"
                                           "time_slice = 60_001"
                                           handler
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [global]"
                   "2: [global.trigger]"
                   "3: sequence_check = 0"
                   "   ^^^^^^^^^^^^^^^^^^"
                   "   It should be boolean value")
           (target/parse-string (str/join "\n"
                                          ["[global]"
                                           "[global.trigger]"
                                           "sequence_check = 0"
                                           handler
                                           trigger]))))))

  (testing "handler"
    (let [trigger
          (str/join "\n"
                    ["[[trigger]]"
                     "name = \"test\""
                     "[[trigger.dbus]]"
                     "bus = \"system\""
                     "type = \"signal\""
                     "path = \"/org/freedesktop/login1/session/**\""
                     "interface = \"org.freedesktop.DBus.Properties\""
                     "member = \"PropertiesChanged\""
                     "[[trigger.handler]]"
                     "name = \"test\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "   ^^^^^^^^^^^"
                   "   At latest one 'handler' table is required")
           (target/parse-string (str/join "\n"
                                          ["[[handler]]"
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "   ^^^^^^^^^^^"
                   "   No 'name' key")
           (target/parse-string (str/join "\n"
                                          ["[[handler]]"
                                           "exec = \"echo 'test'\""
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"\""
                   "   ^^^^^^^^^"
                   "   It should not be empty")
           (target/parse-string (str/join "\n"
                                          ["[[handler]]"
                                           "name = \"\""
                                           "exec = \"echo 'test'\""
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "   ^^^^^^^^^^^"
                   "   No 'exec' key")
           (target/parse-string (str/join "\n"
                                          ["[[handler]]"
                                           "name = \"test\""
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = \"\""
                   "   ^^^^^^^^^"
                   "   It should not be empty")
           (target/parse-string (str/join "\n"
                                          ["[[handler]]"
                                           "name = \"test\""
                                           "exec = \"\""
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = []"
                   "   ^^^^^^^^^"
                   "   It should not be empty")
           (target/parse-string (str/join "\n"
                                          ["[[handler]]"
                                           "name = \"test\""
                                           "exec = []"
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"./project.clj\"]"
                   "   ^^^^^^^^^^^^^^^^^^^^^^^^"
                   "   First item of it should be executable")
           (target/parse-string (str/join "\n"
                                          ["[[handler]]"
                                           "name = \"test\""
                                           "exec = [\"./project.clj\"]"
                                           trigger]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"%h/.local/bin/x\"]"
                   "   ^^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "   First item of it should be executable")
           (target/parse-string (str/join "\n"
                                          ["[[handler]]"
                                           "name = \"test\""
                                           "exec = [\"%h/.local/bin/x\"]"
                                           trigger]))))))

  (testing "trigger"
    (let [handler
          (str/join "\n"
                    ["[[handler]]"
                     "name = \"test\""
                     "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"])

          dbus
          (str/join "\n"
                    ["[[trigger.dbus]]"
                     "bus = \"system\""
                     "type = \"signal\""
                     "path = \"/org/freedesktop/login1/session/**\""
                     "interface = \"org.freedesktop.DBus.Properties\""
                     "member = \"PropertiesChanged\""])

          tgr-handler
          (str/join "\n"
                    ["[trigger.handler]"
                     "name = \"test\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "   ^^^^^^^^^^^"
                   "   At latest one 'trigger' table is required")
           (target/parse-string (str/join "\n"
                                          [handler
                                           "[[trigger]]"]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "   ^^^^^^^^^^^"
                   "   No 'name' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           "[[trigger]]"
                                           dbus
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "   ^^^^^^^^^^^"
                   "   No 'handler' table")
           (target/parse-string (str/join "\n"
                                          [handler
                                           "[[trigger]]"
                                           "name = \"test\""
                                           dbus]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "   ^^^^^^^^^^^"
                   "   No 'dbus' array table")
           (target/parse-string (str/join "\n"
                                          [handler
                                           "[[trigger]]"
                                           "name = \"test\""
                                           tgr-handler]))))))

  (testing "trigger.dbus"
    (let [handler
          (str/join "\n"
                    ["[[handler]]"
                     "name = \"test\""
                     "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"])

          trigger
          (str/join "\n"
                    ["[[trigger]]"
                     "name = \"test\""])

          tgr-handler
          (str/join "\n"
                    ["[trigger.handler]"
                     "name = \"test\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "5: name = \"test\""
                   "6: [[trigger.dbus]]"
                   "   ^^^^^^^^^^^^^^^^"
                   "   At latest one 'dbus' table is required")
           (target/parse-string (str/join "\n"
                                          [handler
                                           "[[trigger]]"
                                           "name = \"test\""
                                           "[[trigger.dbus]]"
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "5: name = \"test\""
                   "6: [[trigger.dbus]]"
                   "   ^^^^^^^^^^^^^^^^"
                   "   No 'bus' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           #_"bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "5: name = \"test\""
                   "6: [[trigger.dbus]]"
                   "   ^^^^^^^^^^^^^^^^"
                   "   No 'type' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           #_"type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "5: name = \"test\""
                   "6: [[trigger.dbus]]"
                   "   ^^^^^^^^^^^^^^^^"
                   "   No 'path' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           #_"path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "5: name = \"test\""
                   "6: [[trigger.dbus]]"
                   "   ^^^^^^^^^^^^^^^^"
                   "   No 'interface' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           #_"interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "1: [[handler]]"
                   "2: name = \"test\""
                   "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                   "4: [[trigger]]"
                   "5: name = \"test\""
                   "6: [[trigger.dbus]]"
                   "   ^^^^^^^^^^^^^^^^"
                   "   No 'member' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           #_"member = \"PropertiesChanged\""
                                           tgr-handler]))))))

  (testing "trigger.dbus.argument"
    (let [handler
          (str/join "\n"
                    ["[[handler]]"
                     "name = \"test\""
                     "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"])

          trigger
          (str/join "\n"
                    ["[[trigger]]"
                     "name = \"test\""])

          tgr-handler
          (str/join "\n"
                    ["[trigger.handler]"
                     "name = \"test\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "7: bus = \"system\""
                   "8: type = \"signal\""
                   "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "    ^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "    At latest one 'argumenet' table is required")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "7: bus = \"system\""
                   "8: type = \"signal\""
                   "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "    ^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "    No 'type' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           #_"type = \"str\""
                                           "value = \"test\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "7: bus = \"system\""
                   "8: type = \"signal\""
                   "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "    ^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "    No 'value' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           "type = \"str\""
                                           #_"value = \"test\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "8: type = \"signal\""
                   "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "13: type = \"custom\""
                   "    ^^^^^^^^^^^^^^^"
                   "    It should be one of \"byte\", \"bool\", \"uint\", \"double\", \"str\" or \"dcit\"")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           "type = \"custom\""
                                           "value = \"test\""
                                           tgr-handler]))))
      (is (thrown-with-msg?
           ExceptionInfo
           (result "7: bus = \"system\""
                   "8: type = \"signal\""
                   "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "    ^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "    'type' and 'value' is not matched")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           "type = \"int\""
                                           "value = \"test\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "13: type = \"str\""
                   "14: value = \"test\""
                   "15: position = -1"
                   "    ^^^^^^^^^^^^^"
                   "    It should be zero or positive integer")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           "type = \"str\""
                                           "value = \"test\""
                                           "position = -1"
                                           tgr-handler]))))))

  (testing "trigger.dbus.argument.dictionary"
    (let [handler
          (str/join "\n"
                    ["[[handler]]"
                     "name = \"test\""
                     "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"])

          trigger
          (str/join "\n"
                    ["[[trigger]]"
                     "name = \"test\""])

          tgr-handler
          (str/join "\n"
                    ["[trigger.handler]"
                     "name = \"test\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "13: type = \"dict\""
                   "14: [[trigger.dbus.argument.dictionary]]"
                   "    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "    At latest one 'dictionary' table is required")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           "type = \"dict\""
                                           "[[trigger.dbus.argument.dictionary]]"
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "13: type = \"dict\""
                   "14: [[trigger.dbus.argument.dictionary]]"
                   "    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "    No 'type' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           "type = \"dict\""
                                           "[[trigger.dbus.argument.dictionary]]"
                                           #_"type = \"str\""
                                           "name = \"test\""
                                           "value = \"test\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "13: type = \"dict\""
                   "14: [[trigger.dbus.argument.dictionary]]"
                   "    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "    No 'name' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           "type = \"dict\""
                                           "[[trigger.dbus.argument.dictionary]]"
                                           "type = \"str\""
                                           #_"name = \"test\""
                                           "value = \"test\""
                                           tgr-handler]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [[trigger.dbus.argument]]"
                   "13: type = \"dict\""
                   "14: [[trigger.dbus.argument.dictionary]]"
                   "    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                   "    No 'value' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           "[[trigger.dbus]]"
                                           "bus = \"system\""
                                           "type = \"signal\""
                                           "path = \"/org/freedesktop/login1/session/**\""
                                           "interface = \"org.freedesktop.DBus.Properties\""
                                           "member = \"PropertiesChanged\""
                                           "[[trigger.dbus.argument]]"
                                           "type = \"dict\""
                                           "[[trigger.dbus.argument.dictionary]]"
                                           "type = \"str\""
                                           "name = \"test\""
                                           #_"value = \"test\""
                                           tgr-handler]))))))

  (testing "trigger.handler"
    (let [handler
          (str/join "\n"
                    ["[[handler]]"
                     "name = \"test\""
                     "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"])

          trigger
          (str/join "\n"
                    ["[[trigger]]"
                     "name = \"test\""])

          dbus
          (str/join "\n"
                    ["[[trigger.dbus]]"
                     "bus = \"system\""
                     "type = \"signal\""
                     "path = \"/org/freedesktop/login1/session/**\""
                     "interface = \"org.freedesktop.DBus.Properties\""
                     "member = \"PropertiesChanged\""])]
      (is (thrown-with-msg?
           ExceptionInfo
           (result "7: bus = \"system\""
                   "8: type = \"signal\""
                   "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [trigger.handler]"
                   "    ^^^^^^^^^^^^^^^^^"
                   "    No 'name' key")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           dbus
                                           "[trigger.handler]"]))))

      (is (thrown-with-msg?
           ExceptionInfo
           (result "9: path = \"/org/freedesktop/login1/session/**\""
                   "10: interface = \"org.freedesktop.DBus.Properties\""
                   "11: member = \"PropertiesChanged\""
                   "12: [trigger.handler]"
                   "13: name = \"test\""
                   "14: delay = -1"
                   "    ^^^^^^^^^^"
                   "    It should be in between 0 and 3_600_000")
           (target/parse-string (str/join "\n"
                                          [handler
                                           trigger
                                           dbus
                                           "[trigger.handler]"
                                           "name = \"test\""
                                           "delay = -1"])))))))


(deftest error-cases
  (testing "when using undefined handler in trigger config"
    (is (thrown-with-msg?
         ExceptionInfo
         (result "8: type = \"signal\""
                 "9: path = \"/org/freedesktop/login1/session/**\""
                 "10: interface = \"org.freedesktop.DBus.Properties\""
                 "11: member = \"PropertiesChanged\""
                 "12: [trigger.handler]"
                 "13: name = \"nonexistent\""
                 "    ^^^^^^^^^^^^^^^^^^^^"
                 "    'nonexistent' is not defined")
         (target/parse-string (str/join "\n"
                                        ["[[handler]]"
                                         "name = \"test\""
                                         "exec = [\"/bin/bash\", \"-c\", \"echo 'test'\"]"
                                         "[[trigger]]"
                                         "name = \"test\""
                                         "[[trigger.dbus]]"
                                         "bus = \"system\""
                                         "type = \"signal\""
                                         "path = \"/org/freedesktop/login1/session/**\""
                                         "interface = \"org.freedesktop.DBus.Properties\""
                                         "member = \"PropertiesChanged\""
                                         "[trigger.handler]"
                                         "name = \"nonexistent\""])))))


  (testing "when using undefined handler in trigger config"
    (is (thrown-with-msg?
         ExceptionInfo
         (result "3: exec = [\"/bin/bash\", \"-c\", \"echo 'test #1'\"]"
                 "4: [[handler]]"
                 "5: name = \"test\""
                 "6: exec = [\"/bin/bash\", \"-c\", \"echo 'test #2'\"]"
                 "7: [[handler]]"
                 "8: name = \"test\""
                 "   ^^^^^^^^^^^^^"
                 "   'test' is already used at line 5")
         (target/parse-string (str/join "\n"
                                        ["[[handler]]"
                                         "name = \"first\""
                                         "exec = [\"/bin/bash\", \"-c\", \"echo 'test #1'\"]"
                                         "[[handler]]"
                                         "name = \"test\""
                                         "exec = [\"/bin/bash\", \"-c\", \"echo 'test #2'\"]"
                                         "[[handler]]"
                                         "name = \"test\""
                                         "exec = [\"/bin/bash\", \"-c\", \"echo 'test #3'\"]"
                                         "[[trigger]]"
                                         "name = \"test\""
                                         "[[trigger.dbus]]"
                                         "bus = \"system\""
                                         "type = \"signal\""
                                         "path = \"/org/freedesktop/login1/session/**\""
                                         "interface = \"org.freedesktop.DBus.Properties\""
                                         "member = \"PropertiesChanged\""
                                         "[trigger.handler]"
                                         "name = \"test\""]))))))
