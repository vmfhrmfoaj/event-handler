(ns event-handler.core-spec
  (:require [clojure.spec.alpha :as spec]))


(spec/def ::simple (spec/and string? (set (map str "simple"))))
