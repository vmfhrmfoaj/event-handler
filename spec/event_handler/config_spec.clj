(ns event-handler.config-spec
  (:require [clojure.java.io :as io]
            [clojure.java.shell :as sh]
            [clojure.spec.alpha :as spec]
            [clojure.spec.alpha.extension :refer [with-explain]]
            [clojure.string :as str]
            [event-handler.config-utils :as conf-utils]))

(set! *warn-on-reflection* true)


(spec/def ::non-blank-str
  (spec/and (with-explain
              string?
              "It should be string")

            (with-explain
              #(not (str/blank? %))
              "It should not be empty")))


(spec/def :config.log/file
  (spec/and ::non-blank-str
            (with-explain
              #(some-> %
                       (io/as-file)
                       (.getParentFile)
                       (.exists))
              "Parent directory of it should be exists")))


(spec/def :config.log/level
  (spec/and ::non-blank-str
            (with-explain
              #{"debug" "info" "warn" "error"}
              "It should be one of \"debug\", \"info\", \"warn\" or \"error\"")))


(spec/def :config/log
  (with-explain
    (spec/keys :opt-un [:config.log/file
                        :config.log/level])
    "It should be table"))


(spec/def :config.handler/delay
  (spec/and (with-explain
              int?
              "It should be integer")

            (with-explain
              #(<= 0 % 3600000)
              "It should be in between 0 and 3_600_000")))


(spec/def :config.handler/retry-after
  (spec/and (with-explain
              int?
              "It should be integer")

            (with-explain
              #(<= % 3600000)
              "It should be in between 0 and 3_600_000 or negative integer")))


(spec/def :config.handler/name
  (spec/and ::non-blank-str))


(spec/def :config.handler/exec
  (spec/and (with-explain
              #(or (sequential? %) (string? %))
              "It should be string array or string")

            (spec/or :executable-file
                     (spec/and sequential?
                               (with-explain
                                 seq
                                 "It should not be empty")

                               (spec/+ (with-explain
                                         string?
                                         "All item in it should be string"))

                               (with-explain
                                 #(some-> % (conf-utils/resolve-special-vars) (first) (io/as-file) (.canExecute))
                                 "First item of it should be executable"))

                     :shell-script
                     (spec/and string?
                               (with-explain #(not (str/blank? %))
                                 "It should not be empty")

                               (with-explain
                                 #(zero? (:exit (sh/sh "/bin/bash" "-n" "-c" %)))
                                 "It should be valid bash script")))))


(spec/def ::handler
  (with-explain
    (spec/keys :req-un [:config.handler/name
                        :config.handler/exec]
               :opt-un [:config.handler/delay
                        :config.handler/retry-after])
    "It should be table"
    "No 'name' key"
    "No 'exec' key"))


(spec/def :config.trigger/interval
  (spec/and (with-explain
              int?
              "It should be integer")

            (with-explain
              #(<= 5 % 1000)
              "It should be in between 5 and 1_000")))


(spec/def :config.trigger/conj
  (spec/and ::non-blank-str
            (with-explain
              #{"and" "or"}
              "It should be \"or\" or \"and\"")))


(spec/def :config.trigger/time-slice
  (spec/and (with-explain
              int?
              "It should be integer")

            (with-explain
              #(<= 100 % 60000)
              "It should be in between 100 and 60_000")))


(spec/def :config.trigger/seq-check?
  (with-explain
    boolean?
    "It should be boolean value"))


(spec/def :config.trigger/quit-on-error?
  (with-explain
    boolean?
    "It should be boolean value"))


(spec/def :config.trigger.dbus/bus
  (spec/and ::non-blank-str
            (with-explain
              #{"system" "session"}
              "It should be 'system' or 'session'")))


(spec/def :config.trigger.dbus/type
  (spec/and ::non-blank-str
            (with-explain
              #{"method-call" "method-return" "error" "signal"}
              "It should be one of \"method-call\", \"method-return\", \"error\" or \"signal\"")))


(spec/def :config.trigger.dbus/path
  ;; TODO
  ;;  if the path is using glob pattern, make sure it is valid
  (spec/and ::non-blank-str
            (with-explain
              #(str/starts-with? % "/")
              "It should starts with '/'")))


(spec/def :config.trigger.dbus/interface
  ::non-blank-str)


(spec/def :config.trigger.dbus/member
  ::non-blank-str)


(spec/def :config.trigger.dbus.arg/type
  (spec/and ::non-blank-str
            (with-explain
              #{"byte" "bool" "int" "double" "str" "dict"}
              "It should be one of \"byte\", \"bool\", \"uint\", \"double\", \"str\" or \"dcit\"")))


(spec/def :config.trigger.dbus.arg/val
  (with-explain
    some?
    "It should not be null"))


(spec/def :config.trigger.dbus.arg/pos
  (spec/and (with-explain
              #(or (int? %) (string? %))
              "It should be integer or string")

            (spec/or :fixed
                     (spec/and int?
                               (with-explain
                                 #(>= % 0)
                                 "It should be zero or positive integer"))

                     :relative
                     (spec/and string?
                               (with-explain
                                 #(not (str/blank? %))
                                 "It should not be empty")

                               (with-explain
                                 #(= "%{continuous}" %)
                                 "It should be \"%{continuous}\"")))))


(spec/def :config.trigger.dbus.arg/var
  ::non-blank-str)


(spec/def :config.trigger.dbus.arg.dict/name
  ::non-blank-str)


(defn- check-dbus-arg-type
  "Returns true if a DBus arguement type is valid.

  Example:
   (check-dbus-arg-type {:type \"bool\" :value \"%{any}\"})"
  [{:keys [type val]}]
  (if (= "%{any}" val)
    true
    (condp = type
      "byte" (instance? Byte val)
      "bool" (instance? Boolean val)
      "str"  (instance? String val)

      "double"
      (or (instance? Float  val)
          (instance? Double val))

      "int"
      (or (instance? Long    val)
          (instance? Integer val)
          (instance? Short   val))

      false)))


(spec/def :config.trigger.dbus.arg/dict
  (with-explain
    (spec/+ (spec/and (with-explain
                        (spec/keys :req-un [:config.trigger.dbus.arg/type
                                            :config.trigger.dbus.arg.dict/name
                                            :config.trigger.dbus.arg/val]
                                   :opt-un [:config.trigger.dbus.arg/var])
                        "It should be table"
                        "No 'type' key"
                        "No 'name' key"
                        "No 'value' key"
                        "No 'variable' key")

                      (with-explain
                        check-dbus-arg-type
                        "'type' and 'value' is not matched")))
    "At latest one 'dictionary' table is required"))


(spec/def :config.trigger.dbus/args
  (with-explain
    (spec/+ (spec/or
             :val
             (spec/and #(not= "dict" (:type %))
                       (with-explain
                         (spec/keys :req-un [:config.trigger.dbus.arg/type
                                             :config.trigger.dbus.arg/val]
                                    :opt-un [:config.trigger.dbus.arg/pos
                                             :config.trigger.dbus.arg/var])
                         "It should be table"
                         "No 'type' key"
                         "No 'value' key")

                       (with-explain
                         check-dbus-arg-type
                         "'type' and 'value' is not matched"))

             :dict
             (spec/and #(= "dict" (:type %))
                       (with-explain
                         (spec/keys :req-un [:config.trigger.dbus.arg/type
                                             :config.trigger.dbus.arg/dict]
                                    :opt-un [:config.trigger.dbus.arg/pos])
                         "It should be table"
                         "No 'type' key"
                         "No 'dictionary' table"))))
    "At latest one 'argumenet' table is required"))


(spec/def ::dbus
  (with-explain
    (spec/keys :req-un [:config.trigger.dbus/bus
                        :config.trigger.dbus/type
                        :config.trigger.dbus/path
                        :config.trigger.dbus/interface
                        :config.trigger.dbus/member]
               :opt-un [:config.trigger.dbus/args])
    "It should be table"
    "No 'bus' key"
    "No 'type' key"
    "No 'path' key"
    "No 'interface' key"
    "No 'member' key"))


(spec/def :config.trigger/dbus
  (with-explain
    (spec/+ ::dbus)
    "At latest one 'dbus' table is required"))


(spec/def :config.trigger/handler-table
  (with-explain
    (spec/keys :req-un [:config.handler/name]
               :opt-un [:config.handler/delay])
    "It should be table"
    "No 'name' key"))


(spec/def :config.trigger/handler
  (spec/or :table
           :config.trigger/handler-table

           :array-table
           (with-explain
             (spec/+ :config.trigger/handler-table)
             "It should be array table")))


(spec/def :config.trigger/name
  ::non-blank-str)


(spec/def ::trigger
  ;; NOTE
  ;;  You can use `and` and `or` in `:req` vector.
  ;;  You will need it add more sources.
  ;;  (e.g. (s/keys :req-un [(or ::a ::b)]))
  (with-explain
    (spec/keys :req-un [:config.trigger/name
                        :config.trigger/dbus
                        :config.trigger/handler]
               :opt-un [:config.trigger/interval
                        :config.trigger/conj
                        :config.trigger/time-slice
                        :config.trigger/seq-check?
                        :config.trigger/quit-on-error?])
    "It should be table"
    "No 'name' key"
    "No 'dbus' array table"
    "No 'handler' table"))


(spec/def :config.global/handler
  (with-explain
    (spec/keys :opt-un [:config.handler/delay])
    "It should be table"))


(spec/def :config.global/trigger
  (with-explain
    (spec/keys :opt-un [:config.trigger/interval
                        :config.trigger/conj
                        :config.trigger/time-slice
                        :config.trigger/seq-check?
                        :config.trigger/quit-on-error?])
    "It should be table"))


(spec/def :config/global
  ;; NOTE
  ;;  you need to check below keys. these keys should not be in this.
  ;;  - [:handler :name]
  ;;  - [:handler :exec]
  ;;  - [:trigger :dbus]
  ;;  - [:trigger :handler]
  (with-explain
    (spec/keys :opt-un [:config.global/handler :config.global/trigger])
    "It should be table"))


(spec/def :config/handler
  (with-explain
    (spec/+ ::handler)
    "At latest one 'handler' table is required"))


(spec/def :config/trigger
  (with-explain
    (spec/+ ::trigger)
    "At latest one 'trigger' table is required"))


(spec/def ::config
  (with-explain
    (spec/keys :req-un [:config/handler
                        :config/trigger]
               :opt-un [:config/log
                        :config/global])
    "It should be table"
    "No 'handler' array table"
    "No 'trigger' array table"))
