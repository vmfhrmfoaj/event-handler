(ns dev.tool
  (:require [clojure.data.xml :as xml]
            [clojure.java.io :as io]
            [clojure.tools.deps :as dep.tool]
            [clojure.tools.deps.util.maven :as dep.mvn])
  (:import clojure.lang.DynamicClassLoader
           java.lang.management.ManagementFactory))


(defn- xml-tag-in
  [tag xml]
  (cond
    (map? xml)
    (if (= tag (:tag xml))
      xml
      (recur tag (:content xml)))
    (sequential? xml)
    (->> xml
         (filter coll?)
         (reduce #(when-let [xml (xml-tag-in tag %2)]
                    (reduced xml))
                 nil))
    :else nil))


(defn- repo []
  (let [mvn-conf-file (io/file (System/getProperty "user.home") ".m2" "settings.xml")
        custom-repos (when (.exists mvn-conf-file)
                       (let [conf (some-> mvn-conf-file (io/reader) (xml/parse))
                             mirrors (some->> conf
                                              (xml-tag-in :settings)
                                              (xml-tag-in :mirrors)
                                              :content)]
                         (some->> mirrors
                                  (filter coll?)
                                  (map (juxt #(some->> % (xml-tag-in :mirrorOf) :content (first))
                                             #(some->> % (xml-tag-in :url)      :content (first) (hash-map :url))))
                                  (into {}))))]
    (merge dep.mvn/standard-repos custom-repos)))


(defn- dyn-class-loader []
  (->> (.getContextClassLoader (Thread/currentThread))
       (iterate #(.getParent %))
       (take-while #(instance? DynamicClassLoader %))
       (last)))


(defn add-lib
  "Add a library dynamically.

  Example:
   (add-lib [\"org.clojure/core.async\" \"1.3.610\"])"
  [[lib ver] & exclusions]
  ;; NOTE
  ;;  Reference implementation `add-libs` in https://github.com/clojure/tools.deps.alpha/blob/add-lib3/src/main/clojure/clojure/tools/deps/alpha/repl.clj
  (let [loader (dyn-class-loader)
        sym (symbol (cond-> lib
                      (not (namespace (symbol lib)))
                      (str "/" lib)))
        deps {sym {:mvn/version ver :exclusions (conj exclusions 'org.clojure/clojure)}}
        lib (dep.tool/resolve-deps {:deps deps :mvn/repos (repo)} nil)
        paths (->> lib
                   (vals)
                   (map :paths)
                   (flatten))]
    (->> paths
         (map io/file)
         (map #(.toURL %))
         (run! #(.addURL loader %)))
    paths))


(defn on-load
  "This function will be called automatically after starting REPL." []
  ;; NOTE
  ;;  If you use Emacs + Cider, you need to add below code snippet to your Emacs config to automatically start servers.
  ;;   ```emacs-lisp
  ;;   (add-hook 'cider-connected-hook
  ;;            (lambda ()
  ;;                    (cider-ns-refresh)
  ;;                    (cider-nrepl-request:eval
  ;;                     (concat "(try"
  ;;                             "  (require 'dev.tool)"
  ;;                             "  (some-> 'dev.tool/on-load (find-var) (.invoke))"
  ;;                             "  (catch Exception _))")
  ;;                     (cider-eval-print-handler)
  ;;                     "dev.tool" nil nil nil
  ;;                     (cider-current-connection))))
  ;;   ````
  )


(defn on-reload
  "This function will be called automatically after starting REPL." []
  ;; NOTE
  ;;  If you use Emacs + Cider, you need to add below code snippet to your Emacs config to automatically start servers.
  ;;   ```emacs-lisp
  ;;   (advice-add #'cider-ns-refresh--handle-response :before
  ;;               (lambda (response log-buffer)
  ;;                 (nrepl-dbind-response response (err status reloading error)
  ;;                   (cond
  ;;                     ((member "ok" status)
  ;;                      (cider-map-repls :clj
  ;;                        (lambda (conn)
  ;;                                (cider-nrepl-request:eval
  ;;                                 (concat "(try"
  ;;                                         "  (require 'dev.tool)"
  ;;                                         "  (some-> 'dev.tool/on-reload (find-var) (.invoke))"
  ;;                                         "  (catch Exception _))")
  ;;                                 (cider-eval-custom-handler)
  ;;                                       "dev.tool" nil nil nil conn))))))))
  ;;   ````
  )


(defn get-jvm-args
  "Return JVM arguments." []
  (.getInputArguments (ManagementFactory/getRuntimeMXBean)))


(comment
  (add-lib ['org.clojure/core.async "1.5.648"])
  )
