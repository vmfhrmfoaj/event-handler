(ns dev.test
  (:require [clojure.spec.alpha :as spec]
            [taoensso.timbre :as log]))


(defn attach-gen!
  "Attaches a generator to a spec.

  Example:
   (attatch-gen ::token #(gen/fmap str (gen/uuid)))"
  [spec gen-fn]
  (and (get (swap! @#'spec/registry-ref (fn [registry]
                                          (cond-> registry
                                            (get registry spec)
                                            (update spec #(spec/with-gen % gen-fn)))))
            spec)
       true))


(defmacro with-hard-redefs
  "Temporarily alters `clojure.lang.Var`s while executing the body that contains asynchronous code.
  At the end of body, you should wait until asynchronous code to complete.

  Example:
   (with-hard-redefs [+ #(apply + 10 %&)]
      (+ 1 2))"
  [bindings & body]
  (when-not (even? (count bindings))
    (throw (ex-info "'binding' should be in pairs" {:input [bindings body]})))

  (let [syms (->> bindings (take-nth 2) (map resolve))
        orig-vals (mapcat (juxt #(gensym (name (symbol %))) #(list 'var-get %)) syms)
        alter-var #(list 'alter-var-root %1 (list 'constantly %2))]
    `(let [~@orig-vals]
       (try
         ~@(->> bindings
                (drop 1)
                (take-nth 2)
                (map alter-var syms))
         ~@body
         (finally
           ~@(->> orig-vals (take-nth 2) (map alter-var syms)))))))


(defn without-trivial-log
  "Set log level to error to ignore debugging logs.

  Example:
   (clojure.test/use-fixtures :once without-trivial-log)"
  [func]
  (log/merge-config! {:min-level :fatal})
  (func))
