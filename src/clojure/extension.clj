(ns clojure.extension)

(set! *warn-on-reflection* true)


(defn safe-nth
  "This function works as same as `nth`.
  But it returns nil instead of throwing an exception. And it takes negative index.

  Examples:
   (safe-nth [0 1 2]  2) ;=> 2
   (safe-nth [0 1 2]  3) ;=> nil
   (safe-nth [0 1 2] -1) ;=> 2"
  [coll index]
  (let [num-of-items (count coll)
        index (if (neg-int? index)
                (+ num-of-items index)
                index)]
    (when (and (>= index 0) (< index num-of-items))
      (nth coll index))))
