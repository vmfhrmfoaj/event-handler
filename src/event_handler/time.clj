(ns event-handler.time
  "Wrapper for `java.time` package."
  (:refer-clojure :exclude [second time])
  (:require [event-handler.error :as err]
            [taoensso.timbre :as log])
  (:import (java.time Duration Instant LocalDateTime Period ZoneId ZoneOffset)
           (java.time.temporal ChronoField ChronoUnit TemporalAmount TemporalUnit)))

(set! *warn-on-reflection* true)

(alias 'my *ns*)


(def ^:const milli-sec-in-sec 1000)

(def ^:const micro-sec-in-sec 1000000)

(def ^:const nano-sec-in-sec  1000000000)

(def ^:const nano-sec-in-milli-sec 1000000)

(def ^:const nano-sec-in-micro-sec 1000)


(def ^{:tag Instant} epoch
  "A time instance for January 1, 1970 UTC."
  (Instant/EPOCH))


(defn now
  "Returns a time instance for now."
  ^Instant []
  (Instant/now))


(defn local-time
  "Returns local time of the given time instance.

  Example:
   (local-time (`now`))"
  ([time]
   (local-time time (ZoneOffset/systemDefault)))
  (^LocalDateTime [^Instant time ^ZoneId zone]
   (LocalDateTime/ofInstant time zone)))


(defmacro ^:pivate defn-get-field
  [name unit-a unit-b]
  (let [fn-name (symbol name)]
    `(defn ~fn-name
       ~(format "Get %ss of given time instant.

  Example:
   (%s (`now`))" name name)
       [x#]
       (cond
         (instance? Instant x#)
         (recur (local-time x# ZoneOffset/UTC))

         (instance? LocalDateTime x#)
         (.get ^LocalDateTime x# ~unit-a)

         (instance? Period x#)
         (.get ^Period x# ~unit-b)))))

(defn-get-field "year"   ChronoField/YEAR                  ChronoUnit/YEARS)
(defn-get-field "month"  ChronoField/MONTH_OF_YEAR         ChronoUnit/MONTHS)
(defn-get-field "week"   ChronoField/ALIGNED_WEEK_OF_MONTH ChronoUnit/WEEKS)
(defn-get-field "day"    ChronoField/DAY_OF_MONTH          ChronoUnit/DAYS)
(defn-get-field "hour"   ChronoField/HOUR_OF_DAY           ChronoUnit/HOURS)
(defn-get-field "minute" ChronoField/MINUTE_OF_HOUR        ChronoUnit/MINUTES)


(defn second
  "Get seconds of given time instant.

  Examples:
   (second (`now`))
   (second (`now`) :sec)
   (second (`now`) :milli)
   (second (`now`) :micro)
   (second (`now`) :nano)"
  ([x]
   (second x :default))
  ([x unit]
   (cond
     (instance? Instant x)
     (recur (local-time x ZoneOffset/UTC) unit)

     (instance? LocalDateTime x)
     (let [sec      (.get ^LocalDateTime x ChronoField/SECOND_OF_MINUTE)
           nano-sec (.get ^LocalDateTime x ChronoField/NANO_OF_SECOND)]
       (condp = unit
         :default (+ sec (double (/ nano-sec nano-sec-in-sec)))
         :milli (+ (* sec milli-sec-in-sec) (long (/ nano-sec nano-sec-in-milli-sec)))
         :micro (+ (* sec micro-sec-in-sec) (long (/ nano-sec nano-sec-in-micro-sec)))
         :nano  (+ (* sec nano-sec-in-sec)  nano-sec)
         :sec sec
         (do
           (log/error "unknown unit:" unit)
           (throw (ex-info "unknown unit"
                           {:type err/illegal-args
                            :input [x unit]
                            :available-units [:nano :micro :milli :sec nil]})))))

     (instance? Duration x)
     (let [sec      (.get ^Duration x ChronoUnit/SECONDS)
           nano-sec (.get ^Duration x ChronoUnit/NANOS)]
       (condp = unit
         :default (+ sec (double (/ nano-sec nano-sec-in-sec)))
         :milli (+ (* sec milli-sec-in-sec) (long (/ nano-sec nano-sec-in-milli-sec)))
         :micro (+ (* sec micro-sec-in-sec) (long (/ nano-sec nano-sec-in-micro-sec)))
         :nano  (+ (* sec nano-sec-in-sec)  nano-sec)
         :sec sec
         (do
           (log/error "unknown unit:" unit)
           (throw (ex-info "unknown unit"
                           {:type err/illegal-args
                            :input [x unit]
                            :available-units [:nano :micro :milli :sec nil]}))))))))


(defn time
  "Returns a time instance.

  Example:
   (time :year 1970 :month 01 :day 01 :hour 00 :minute 00 :second 00)
   (time :year 1970 :month 01 :day 01)                     ; use hours, minutes and seconds of the current time
   (time :year 1970 :month 01 :day 01 :base-time (`epoc`)) ; or you can give the time instead of the current time"
  [& {:keys [year month day hour minute second base-time] :or {base-time (now)}}]
  (let [now (local-time base-time ZoneOffset/UTC)
        year   (or year   (my/year   now))
        month  (or month  (my/month  now))
        day    (or day    (my/day    now))
        hour   (or hour   (my/hour   now))
        minute (or minute (my/minute now))
        second (or second (my/second now))
        nanosecond (* (- second (int second)) nano-sec-in-sec)]
    (.toInstant (LocalDateTime/of (int year) (int month) (int day) (int hour) (int minute) (int second) (int nanosecond)) ZoneOffset/UTC)))


(defn between
  "Returns a duration instance.

  Example:
   (between (`now`) (`plus` (`now`) 30 :second))"
  [^Instant t1 ^Instant t2]
  (Duration/between t1 t2))


(def ^:private kw->unit
  {:nano   ChronoUnit/NANOS
   :micro  ChronoUnit/MICROS
   :milli  ChronoUnit/MILLIS
   :second ChronoUnit/SECONDS
   :minute ChronoUnit/MINUTES
   :hour   ChronoUnit/HOURS
   :day    ChronoUnit/DAYS
   :week   ChronoUnit/WEEKS
   :month  ChronoUnit/MONTHS
   :year   ChronoUnit/YEARS})

(def ^{:doc (str "Available units:\n"
                 (->> (keys kw->unit)
                      (map #(str "  - " %))
                      (interpose "\n")
                      (apply str)))}
  available-units
  (keys kw->unit))

(defn- unit
  "Returns TemporalUnit object.
  Available aliaes: see `kw->unit`

  Examples:
   (unit :second) ; be careful, :second is an alias for ChronoUnit/NANOS
   (unit `ChronoUnit/NANOS`)"
  ^TemporalUnit [unit]
  (cond
    (instance? TemporalUnit unit) unit

    (keyword? unit)
    (if-let [unit (kw->unit unit)]
      unit
      (throw (ex-info "unknow alias" {:alias unit :available-aliaes available-units})))

    :else (throw (ex-info "unsupported type" {:input unit :type (type unit)}))))


(defn plus
  "Returns a copy of given time instance with the specified amount added.
  Available units: `available-units`

  Examples:
   (plus (`between` (`now`) (`now`)))
   (plus (`now`) 1 :second)"
  (^Instant [^Instant time duration]
   (.plus time ^TemporalAmount duration))
  (^Instant [^Instant time offset unit]
   (if (= :second unit)
     (.plus time (long (* nano-sec-in-sec offset)) (my/unit :nano))
     (.plus time (long offset) (my/unit unit)))))


(defn minus
  "Returns a copy of given time instance with the specified amount subtraced.
  Available units: `available-units`

  Examples:
   (minus (`between` (`now`) (`now`)))
   (minus (`now`) 1 :second)"
  (^Instant [^Instant time duration]
   (.minus time ^TemporalAmount duration))
  (^Instant [^Instant time offset unit]
   (if (= :second unit)
     (.minus time (long (* nano-sec-in-sec offset)) (my/unit :nano))
     (.minus time (long offset) (my/unit unit)))))


(defn truncate
  "Returns a copy of given time instance that truncated to specified time unit.

  Example:
   (truncate (`now`) :second) ; set fields smaller than the second to 0."
  ^Instant [^Instant time unit]
  (.truncatedTo time (my/unit unit)))


(def before?
  "Returns true if ...

  Example:
   (before? (`plus` (`now`) 1 :second) (`now`))"
  #(.isBefore ^Instant %1 %2))


(def after?
  "Returns true if ...

  Example:
   (after? (`now`) (`plus` (`now`) 1 :second))"
  #(.isAfter ^Instant %1 %2))
