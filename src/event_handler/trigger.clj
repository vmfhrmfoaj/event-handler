(ns event-handler.trigger
  (:require [clojure.core.async :as async :refer [go <! >!]]
            [event-handler.dbus.core :as dbus]
            [event-handler.dbus.msg :as dbus-msg]
            [event-handler.handler :as handler]
            [event-handler.time :as time]
            [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)


(defn dbus-source-fn
  "Return a function that take a DBus message and check if the message meets a given specification.
  If the function passes the check, it returns a map containing captured values otherwise nil.

  Example:
   (dbus-trigger-fn {:type :signal
                     :path \"/org/freedesktop/login1/session/**\"
                     :interface \"org.freedesktop.DBus.Properties\"
                     :member \"PropertiesChanged\"
                     :args [{:type :str
                             :val \"org.freedesktop.login1.Session\"}
                            {:type :dict
                             :val [{:type :bool
                                    :name \"LockedHint\"
                                    :val \"%{any}\"
                                    :var \"is-lock\"}]
                             :pos \"%{continuous}\"}]})"
  [{:keys [type path interface member args] :as _dbus}]
  (let [check-hdr (every-pred (dbus-msg/msg-type-check-fn      type)
                              (dbus-msg/msg-path-check-fn      path)
                              (dbus-msg/msg-interface-check-fn interface)
                              (dbus-msg/msg-member-check-fn    member))
        ;; TODO
        ;;  how to represent a case where there is no arguments?
        get-var-tbl (or (dbus-msg/vars-extraction-fn args) (constantly {}))]
    (fn [msg]
      (when (check-hdr msg)
        ;; TODO
        ;; (log/debug "found dbus msg that matched header:" {:msg msg})
        (some->> (get-var-tbl msg)
                 (merge {"dbus.type"      (dbus-msg/msg-type      msg)
                         "dbus.path"      (dbus-msg/msg-path      msg)
                         "dbus.interface" (dbus-msg/msg-interface msg)
                         "dbus.member"    (dbus-msg/msg-member    msg)}))))))


(defn- elpase-time-in-ms
  ([time]
   (elpase-time-in-ms time (time/now)))
  ([time-a time-b]
   (time/second (time/between time-a time-b) :milli)))


(defn- sequential-time?
  [[time & times]]
  (and (reduce (fn [prev-time time]
                 (if (time/after? time prev-time)
                   time
                   (reduced nil)))
               time
               times)
       true))


(defn- oldest-&-latest-time
  [[time & times]]
  (reduce (fn [[oldest latest] time]
            (cond
              (time/before? time oldest) [time   latest]
              (time/after? time latest)  [oldest time]
              :else                      [oldest latest]))
          [time time]
          times))


(defn- send-event
  "Send an event to handlers."
  [handler-ch handlers var-tbl]
  (doseq [{:keys [name delay]} handlers]
    (let [ev (handler/event name var-tbl delay)]
      (log/info "send an event:" ev)
      (async/put! handler-ch ev))))


(defn trigger-fn
  "Return a function that take trigger id and a map containing values that captured from a trigger source.
  When all conditions in a given specification are meet, the function sends an event with the map to handler.

  Example:
   (trigger-fn {:name \"Screen (un)lock\"
                :interval 1000
                :conj :or
                :time-slice 100
                :seq-check? false
                :sources [{:type :dbus, :data {...}}, ...]
                :handlers [{:name \"change-cpu-freq-policy\" :delay 500}]}
    (`aysnc/chan`))"
  [{:keys [name interval conj time-slice seq-check? sources handlers] :as _trigger} handler-ch]

  ;; TODO
  ;;  creates functions (e.g., `interval`, `conjunction`, `time-slice` and so on functions),
  ;;  and then composes these functions

  (let [num-srcs (count sources)
        empty-fire-time (->> (range num-srcs)
                             (map #(vector % nil))
                             (into (if seq-check? (sorted-map) {})))
        state (atom {:last-sent-time time/epoch
                     :fire-time empty-fire-time
                     :var-tbl nil})]
    (fn [src-id var-tbl]
      (let [now (time/now)
            elpase-time (elpase-time-in-ms (:last-sent-time @state))]
        (cond
          (< elpase-time interval)
          (log/info (str "'" name "'") "is inactivated for" (- interval elpase-time) "msecs, interval =" interval "msecs")

          (= :or conj)
          (do
            (swap! state assoc :last-sent-time now)
            (send-event handler-ch handlers var-tbl))

          (not (and (int? src-id) (>= src-id 0)))
          (log/warn (str "source id(" src-id ")") "is invalid")

          :else
          (let [fire-times (-> (swap! state #(-> %
                                                 (assoc-in [:fire-time src-id] now)
                                                 (update :var-tbl merge var-tbl)))
                               (:fire-time)
                               (vals))]
            (cond
              (some nil? fire-times) nil

              (< time-slice (apply elpase-time-in-ms (oldest-&-latest-time fire-times)))
              (log/debug "all msgs were founded, but all of them is not within a time window, so skip it.")

              (and seq-check? (not (sequential-time? fire-times)))
              (do
                (log/debug "all matching msgs were founded, but they are out of order, so skip it.")
                (swap! state assoc :fire-time empty-fire-time :var-tbl nil))

              :else
              (let [var-tbl (:var-tbl @state)]
                (swap! state assoc
                       :last-sent-time now
                       :fire-time empty-fire-time
                       :var-tbl nil)
                (send-event handler-ch handlers var-tbl)))))))))


(def ^:private source (atom {}))


(defn- inc-ref-count
  [{:keys [count] :as src}]
  (when src
    (assoc src :count (inc (or count 0)))))


(defn- dec-ref-count
  [{:keys [count] :as src}]
  (when src
    (assoc src :count (dec (or count 1)))))


(defn- source-type
  [{:keys [type data]}]
  (condp = type
    :dbus [:dbus (:bus data)]
    nil))


(defn- disconnect-source
  [src-type {:keys [multi data]}]
  (when multi
    (try
      (async/close! multi)
      (catch Exception _e)))
  (when data
    (cond
      (or (= [:dbus :session] src-type)
          (= [:dbus :system]  src-type))
      (do
        (dbus/disconnect data)
        true)
      :else
      (do
        (log/warn "unknown source type:" src-type)
        nil))))


(defn disconnect-source!
  "Close a cached source, and always return nil.

  Example:
   (disconnect-source! [:dbus :system] {:src-data {:ch (`async/chan`), ...}, :multi (`async/mult`) ...})"
  ([src]
   (disconnect-source! (source-type src) src))
  ([src-type src]
   (when (disconnect-source src-type src)
     (swap! source dissoc src-type)
     nil)))


(defn connect-source!
  "Connect to a source and cache it to reuse it if possible.

  Examples:
   (connect-source! [:dbus :system]  #(fn [e] ...))
   (connect-source! [:dbus :session] #(fn [e] ...) (`async/chan`)) ; you can use your own channel instead of"
  ([src-type report-err]
   (connect-source! src-type
                    report-err
                    (-> (Runtime/getRuntime)
                        (.availableProcessors)
                        (async/buffer)
                        (async/chan))))
  ([src-type report-err ch]
   (if-let [multi (get-in @source [src-type :multi])]
     (try
       (async/tap multi ch)
       (swap! source update src-type inc-ref-count)
       ch
       (catch Exception e
         ;; TODO
         ;;  - enhance an error message
         (log/error "unable to subscribe to the source" {:source-type src-type :error e})
         nil))
     (let [src-data (condp = src-type
                      [:dbus :session] (dbus/connect :session report-err)
                      [:dbus :system]  (dbus/connect :system  report-err))
           multi (async/mult (:ch src-data))]
       (swap! source assoc src-type {:data src-data :multi multi :count 0})
       (recur src-type report-err ch)))))


(defn disconnect-all-sources!
  "Close all cached sources, and always return nil." []
  (doseq [[src-type {:keys [source]}] @source]
    (disconnect-source src-type source))
  (reset! source {})
  nil)


(defn- start-dbus-src
  [src-id {:keys [bus] :as conf} dbus-ch trigger-ch]
  (let [get-var-tbl-if-match (dbus-source-fn conf)]
    (go
      (loop []
        (when-let [msg (<! dbus-ch)]
          (when-let [var-tbl (get-var-tbl-if-match msg)]
            (log/debug "got dbus msg:" {:msg msg :var-tbl var-tbl})
            (>! trigger-ch [src-id var-tbl]))
          (recur)))
      (swap! source update [:dbus bus] dec-ref-count))))


(defn start
  "Run a go-block to receive events from sources and send a signal to the handler receiver if matching event was found.
  And return a channel to control the go-block.

  Example:
   (start {:name \"Screen (un)lock\"
           :interval 1000
           :conj :or
           :time-slice 100
           :seq-check? false
           :sources [{:type :dbus, :data {...}}, ...]
           :handlers [{:name \"change-cpu-freq-policy\"}]}
          (`async/chan`)
          (`async/chan`))"
  ([{:keys [sources] :as trigger} err-ch handler-ch]
   (start trigger err-ch handler-ch (async/chan (async/sliding-buffer (count sources)))))
  ([{:keys [name] :as trigger} err-ch handler-ch trigger-ch]
   (let [send-event-if-match-tgr (trigger-fn trigger handler-ch)
         src-chs (->> (:sources trigger)
                      (map-indexed vector)
                      (reduce (fn [src-chs [src-id {:keys [type data] :as src}]]
                                (let [err-report-fn (comp (partial async/put! err-ch)
                                                          (partial apply ex-info)
                                                          (juxt ex-message
                                                                (comp #(assoc % :trigger trigger :source-id src-id) ex-data)
                                                                (memfn ^RuntimeException getCause)))
                                      src-ch (connect-source! (source-type src) err-report-fn)]
                                  (if (condp = type
                                        :dbus
                                        (do
                                          (start-dbus-src src-id data src-ch trigger-ch)
                                          true)

                                        (do
                                          (log/error "unknown source type:" type)
                                          (log/info "clean up sources already connected")
                                          (doseq [ch src-chs]
                                            (async/close! ch))
                                          nil))
                                    (conj src-chs src-ch)
                                    (reduced nil))))
                              []))]
     (when-not src-chs
       (throw (ex-info (str "unable to start '" name "' trigger") {:input [trigger handler-ch trigger-ch]})))

     (go
       (log/info "trigger" (str "'" name "'") "is starting")
       (loop []
         (when-let [[src-id var-tbl] (<! trigger-ch)]
           (send-event-if-match-tgr src-id var-tbl)
           (recur)))

       (doseq [ch src-chs]
         (async/close! ch))
       (log/info "trigger" (str "'" name "'") "was stopped"))

     {:trigger trigger
      :source-chs src-chs
      :ch trigger-ch})))


(defn stop
  "Stop the handler receiver.

  Example:
   (stop {:trigger {:name \"...\", ...} :source-chs [(`async/chan`) ...] :ch (`aysnc/chan`)})"
  [{:keys [trigger source-chs ch]}]
  (log/info "stop" (str "'" (:name trigger) "'") "trigger")
  (doseq [src-ch (filter some? source-chs)]
    (async/close! src-ch))
  (when ch
    (async/close! ch)))
