(ns event-handler.error
  (:require [clojure.core.async :as async]))

(set! *warn-on-reflection* true)


(def ^:const conf-not-found "Configuration file is not exists." ::config-file-not-found)

(def ^:const conf-syntax-err "Configuration syntax error." ::config-syntax-err)

(def ^:const invalid-conf "Invalid configuration." ::invalid-config)

(def ^:const io-error "IO error." ::io-error)

(def ^:const illegal-args "Illegal arguements" ::illegal-arguements)

(def ^:const unable-reocvery "Unable to recovery" ::unable-reocvery)
