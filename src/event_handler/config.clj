(ns event-handler.config
  (:require [clj-toml.core :as toml]
            [clojure.java.io :as io]
            [clojure.set :refer [rename-keys]]
            [clojure.spec.alpha :as spec]
            [clojure.string :as str]
            [clojure.walk :as walk]
            [event-handler.error :as err]
            [event-handler.config-error :as conf-err]
            [event-handler.config-utils :refer :all]
            [event-handler.config-spec :as conf-spec])
  (:import clojure.lang.ExceptionInfo))

(set! *warn-on-reflection* true)


(def ^{:private true :const true} default-log-file               "Default value for log file."               "/dev/stdout")

(def ^{:private true :const true} default-log-level              "Default value for log level."              :warn)

(def ^{:private true :const true} default-handler-delay          "Default value for delay of handler."       10)

(def ^{:private true :const true} default-handler-retry-after    "Default value for retry-after of handler." -1)

(def ^{:private true :const true} default-trigger-interval       "Default value for interval of trigger."    200)

(def ^{:private true :const true} default-trigger-conj           "Default value for conjunction of trigger." :or)

(def ^{:private true :const true} default-trigger-time-slice     "Default value for time-slice of trigger."  500)

(def ^{:private true :const true} default-trigger-seq-check      "Default value for seq-check of trigger."   false)

(def ^{:private true :const true} default-trigger-quit-on-error  "Default value for quit-on-err of trigger." true)


(def ^:private trans-map
  {"argument"       "args"
   "bind_to"        "var"
   "conjunction"    "conj"
   "dictionary"     "dict"
   "position"       "pos"
   "quit_on_error"  "quit-on-err?"
   "retry_after"    "retry-after"
   "sequence_check" "seq-check?"
   "time_slice"     "time-slice"
   "value"          "val"})


(defn- rename-key
  "Rename a string key using `trans-map`.

  Example:
   (rename-key \"bind_to\")"
  [key]
  (trans-map key key))


#_(def ^:private restore-key
    (let [m (reduce-kv #(assoc %1 (name %3) %2) {} trans-map)]
      #(m % %)))


(defn- keywordize
  "Takes a map, convert keys in the map to the keyword and return it.

  Example:
   (keywordize {\"key\" 10})"
  [conf]
  (walk/postwalk #(cond->> %
                    (map? %)
                    (reduce (fn [m [k v]]
                              (let [k (cond-> k
                                        (string? k)
                                        (-> (rename-key) (keyword)))]
                                (assoc m k v)))
                            {}))
                 conf))


(defn- no-neg
  "Returns the given value if the value is not negative number, otherwise nil.

  Example:
   (no-neg  0) ;;=> 0
   (no-neg -1) ;;=> nil"
  [val]
  (when (not (neg? val))
    val))


(defn- update-handler
  "TODO"
  [conf _md handler]
  (-> handler
      (update :delay       (fnil identity (get-in conf [:global :handler :delay]       default-handler-delay)))
      (update :retry-after (fnil no-neg   (get-in conf [:global :handler :retry-after] default-handler-retry-after)))
      (update :exec #(cond
                       (string? %) ["/bin/bash" "-c" %]
                       (vector? %) (resolve-special-vars %)
                       :else %))))


(defn- update-dbus-arg
  "TODO"
  [arg]
  (mapv (fn [arg]
          (let [dict (:dict arg)
                arg (-> arg
                        (dissoc :dict)
                        (update :type keyword))]
            (cond-> arg
              (= :dict (:type arg))
              (assoc :val (update-dbus-arg dict)))))
        arg))


(defn- update-trigger
  "TODO"
  [conf md trigger]
  (let [dbus-poses (map (fn [[k {::toml/keys [start]}]]
                          [start [:dbus k]])
                        (:dbus md))
        all-poses (sort-by first (concat dbus-poses #_sources...))
        dbus (->> (:dbus trigger)
                  (mapv #(hash-map :type :dbus
                                   :data (-> %
                                             (update :bus  keyword)
                                             (update :type keyword)
                                             (update :args update-dbus-arg)))))]
    (-> trigger
        (dissoc :dbus)
        (update :interval       (fnil identity (get-in conf [:global :trigger :interval]       default-trigger-interval)))
        (update :conj           (fnil keyword  (get-in conf [:global :trigger :conj]           default-trigger-conj)))
        (update :time-slice     (fnil identity (get-in conf [:global :trigger :time-slice]     default-trigger-time-slice)))
        (update :seq-check?     (fnil identity (get-in conf [:global :trigger :seq-check?]     default-trigger-seq-check)))
        (update :quit-on-error? (fnil identity (get-in conf [:global :trigger :quit-on-error?] default-trigger-quit-on-error)))
        (update :handler #(cond-> % (map? %) (vector)))
        (assoc :sources (reduce (fn [v [_ [src pos]]] ; flatten triggers and sort by the position
                                  (conj v (condp = src
                                            :dbus (dbus pos))))
                                []
                                all-poses))
        (rename-keys {:handler :handlers}))))


(defn- using-undefined-handler-err-msg
  "Returns an error message if there are trigger that using undefined handler.

  Example:
   (let [conf-str (slurp \"config.toml\")
         raw-conf (`toml/parse-string` conf-str)]
     (using-undefined-handler-err-msg (`keywordize` raw-conf) conf-str (`keywordize` (meta raw-conf))))"
  [conf conf-str md]
  (let [handler? (into #{} (map :name (:handler conf)))
        trigger-md (:trigger md)
        error (fn [handler-name loc]
                (when-not (handler? handler-name)
                  (let [{::toml/keys [start end]} (get-in trigger-md loc)]
                    (reduced (cond->> (str "'" handler-name "' is not defined")
                               (and start end)
                               (conf-err/err-msg-with-ctx conf-str start end))))))]
    (->> (:trigger conf)
         (map-indexed vector)
         (reduce (fn [_ [i {:keys [handler]}]]
                   (cond
                     (map? handler) (error (:name handler) [i :handler :name])
                     (sequential? handler)
                     (doseq [[j handler] (map-indexed vector handler)]
                       (error (:name handler) [i :handler j :name]))))
                 nil))))


(defn- duplicated-handler-error-msg
  "Returns an error message if there are duplicated handlers.

  Example:
   (let [conf-str (slurp \"config.toml\")
         raw-conf (`toml/parse-string` conf-str)]
     (duplicated-handler-error-msg (`keywordize` raw-conf) conf-str (`keywordize` (meta raw-conf)))) "
  [conf conf-str md]
  (let [handler-md (:handler md)]
    (loop [pos {}
           i 0
           [{:keys [name]} & handlers] (:handler conf)]
      (cond
        (nil? name) nil
        (not (pos name)) (recur (assoc pos name (get-in handler-md [i :name])) (inc i) handlers)
        :else
        (let [prev-line-num (some->> name (pos) (::toml/start) (subs conf-str 0) (str/split-lines) (count) (inc))
              {::toml/keys [start end]} (get-in handler-md [i :name])]
          (cond->> (str "'" name "' is already used at line " prev-line-num)
            (and start end)
            (conf-err/err-msg-with-ctx conf-str start end)))))))


(defn parse-string
  "Parse config string.

  Example:
   (parse-string \"[log]\n...\")"
  [conf-str]
  (let [raw-conf (try
                   (toml/parse-string conf-str)
                   (catch ExceptionInfo e
                     (let [{:keys [line column text input]} (ex-data e)]
                       (if-not (and line column text input)
                         (throw e)
                         (let [line (dec line)
                               start (->> input (str/split-lines) (take line) (map count) (reduce +) (+ line))
                               end (+ start column)
                               err-msg (conf-err/err-msg-with-ctx input start end "invalid syntax")]
                           (throw (ex-info err-msg {:type err/conf-syntax-err :input [conf-str]} e)))))))
        md   (keywordize (meta raw-conf))
        conf (keywordize raw-conf)]

    (when-let [reports (some->> conf (spec/explain-data ::conf-spec/config) (::spec/problems) (map #(conf-err/report md %)))]
      (throw (ex-info (conf-err/summary conf-str reports) {:type err/invalid-conf :input [conf-str] :reports reports})))

    (when-let [err-msg (using-undefined-handler-err-msg conf conf-str md)]
      (throw (ex-info err-msg {:type err/invalid-conf :input [conf-str]})))

    (when-let [err-msg (duplicated-handler-error-msg conf conf-str md)]
      (throw (ex-info err-msg {:type err/invalid-conf :input [conf-str]})))

    (-> conf
        (dissoc :global)
        (update-in [:log :file]  (fnil identity default-log-file))
        (update-in [:log :level] (fnil keyword  default-log-level))
        (update :handler (partial mapv #(update-handler conf (get-in md [:handler %1]) %2) (range)))
        (update :trigger (partial mapv #(update-trigger conf (get-in md [:trigger %1]) %2) (range)))
        (rename-keys {:handler :handlers :trigger :triggers}))))


(defn load-conf-file
  "Load configuration from a file.

  Example:
   (parse-config-file \"dev-resources/config.example.toml\")"
  [path]
  (when-not (and path (.exists (io/as-file path)))
    (throw (ex-info (str path " is not exists") {:type err/conf-not-found :input [path]})))
  (try
    (parse-string (slurp path))
    (catch ExceptionInfo e
      (throw (ex-info (ex-message e)
                      (->> (ex-data e)
                           (:reports)
                           (map #(assoc % :file path))
                           (hash-map :problems)
                           (into {:type err/invalid-conf :input [path]}))
                      e)))))
