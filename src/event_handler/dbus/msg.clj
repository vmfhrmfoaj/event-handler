(ns event-handler.dbus.msg
  (:require [clojure.extension :refer :all]
            [event-handler.dbus.marshalling :as m]
            [event-handler.dbus.internal.utils :refer :all]
            [taoensso.timbre :as log])
  (:import org.freedesktop.dbus.DBusMap
           org.freedesktop.dbus.types.Variant
           (org.freedesktop.dbus.messages DBusSignal
                                          Message
                                          MethodCall
                                          #_MethodReturn)))

(set! *warn-on-reflection* true)


(def ^:dynamic *bus-name*
  "DBus bus name."
  "org.freedesktop.DBus")


(defn method-call
  "Return a DBus message of method-call type.

  Example:
   (method-call \"/org/freedesktop/DBus\" \"org.freedesktop.DBus\" \"AddMatch\" [\"Type='signal'\"])"
  ([path interface member]
   (method-call path interface member nil nil nil))
  ([path interface member vals]
   (method-call path interface member (apply m/sig vals) vals nil))
  ([path interface member sig vals]
   (method-call path interface member sig vals nil))
  ([path interface member sig vals opt]
   (let [flag (m/flag opt)
         vals (into-array java.lang.Object vals)]
     (MethodCall. *bus-name* path interface member flag sig (m/dbus-vals vals)))))


(defn method-return
  "Return a DBus message of method-return type.

  Example:
   (method-return ...)"
  [_msg-id _vals]
  (throw (Exception. "not implemented"))
  )


(defn signal
  "Return a DBus message of signal type.

  Example:
   (signal \"/org/freedesktop/login1/session/_32\"
           \"org.freedesktop.DBus.Properties\"
           \"PropertiesChanged\"
           [\"org.freedesktop.login1.Session\" {\"LockedHint\" true}])"
  ([path interface member]
   (signal path interface member nil nil))
  ([path interface member vals]
   (signal path interface member (apply m/sig vals) vals))
  ([path interface member sig vals]
   (let [vals (into-array java.lang.Object vals)]
     (DBusSignal. *bus-name* path interface member sig (m/dbus-vals vals)))))


(defn error
  "Return a DBus message of error type.

  Example:
   (error ...)"
  [_err-name _msg-id _vals]
  (throw (Exception. "not implemented"))
  )


(defn msg-type
  "Extract 'type' from a DBus message.

  Example:
   (msg-type #obj[`Message`])"
  [^Message msg]
  (when msg
    (.getType msg)))


(def ^:const method-call-type   "METHOD_CALL message type."   1)

(def ^:const method-return-type "METHOD_RETURN message type." 2)

(def ^:const error-type         "ERROR message type."         3)

(def ^:const signal-type        "SIGNAL message type."        4)


(def msg-type->kw
  "Convert Dbus message type to the keyword.

  Example:
   (msg-type->kw (`msg-type` #obj[`Message`]))"
  {method-call-type   :method-call
   method-return-type :method-return
   error-type         :error
   signal-type        :signal})


(def kw->msg-type
  "Convert Dbus message type to the keyword.

  Example:
   (kw->msg-type (`msg-type->kw` (`msg-type` #obj[`Message`])))"
  {:method-call   method-call-type
   :method-return method-return-type
   :error         error-type
   :signal        signal-type})


(defn msg-type-check-fn
  "Return a function that takes a keyword/number for DBus type and returns true if a type of a DBus message equals to the input.

  Examples:
   ((msg-type-check-fn `SIGNAL-TYPE`) #obj[`Message`]) ;= ((msg-type-check-fn :signal) #obj[`Message`])"
  [typ]
  (comp (partial = (cond-> typ
                     (keyword? typ)
                     (kw->msg-type typ)))
        msg-type))


(defn msg-path
  "Extract 'path' from a DBus message.

  Example:
   (msg-path #obj[`Message`])"
  [^Message msg]
  (when msg
    (.getPath msg)))


(defn msg-path-check-fn
  "Return a function that takes a glob/normal string for DBus path and returns true
  if a path of a DBus message equals/matches to the input.

  Examples:
   ((msg-path-check-fn \"/org/freedesktop/login1/session/_32\") #obj[`Message`])
   ((msg-path-check-fn \"/org/freedesktop/login1/session/*\")   #obj[`Message`])"
  [path]
  (comp (if (re-find #"(?<=^|[^\\])[*?{}\[\]]" path)
          (partial re-find (glob->regex path))
          (partial = path))
        msg-path))


(defn msg-interface
  "Extract 'interface' from a DBus message.

  Example:
   (msg-interface #obj[`Message`])"
  [^Message msg]
  (when msg
    (.getInterface msg)))


(defn msg-interface-check-fn
  "Return a function that takes a string for DBus interface and returns true if a interface of a DBus message equals to the input.

  Example:
   ((msg-interface-check-fn \"org.freedesktop.DBus.Properties\") #obj[`Message`])"
  [interface]
  (comp (partial = interface) msg-interface))


(defn msg-member
  "Extract 'member' from a DBus message.

  Example:
   (msg-member #obj[`Message`])"
  [^Message msg]
  (when (and msg (not= (msg-type msg) error-type))
    (.getName msg)))


(defn msg-member-check-fn
  "Return a function that takes a string for DBus member and returns true if a member of a DBus message equals to the input.

  Example:
   ((msg-member-check-fn \"PropertiesChanged\") #obj[`Message`])"
  [member]
  (comp (partial = member) msg-member))


(defn msg-vals-sig
  "Extract a DBus signature of 'body' from a DBus mesage.

  Example:
   (msg-vals-sig #obj[`Message`])"
  [^Message msg]
  (when msg
    (.getSig msg)))


(defn msg-vals
  "Extract 'body' from a DBus message.

  Example:
   (msg-vals #obj[`Message`])"
  [^Message msg]
  (when-let [vals (and msg (.getParameters msg))]
    ;; TODO
    ;;  convert a value to Clojure data strucuture if a type of the value is `array` and `dictionary`
    vals
    ))


(defn- spec->sig
  "Convert a value specfication to DBus signature.

  Example:
   (spec->sig {:type :byte})"
  [{:keys [type]}]
  (condp = type
    :byte   "y"
    :bool   "b"
    :int    ["n" "i" "x"]
    :double "d"
    :str    "s"
    :dict   "a{sv}"))


(declare spec->vars-extraction-fn)


(defn- dict-spec->var-extraction-fn
  "Generate a function that extract a variable according to a given dictionary value specification.

  Example:
   ((dict-spec->var-extraction-fn {:type :bool :name \"LockedHint\" :val \"%{any}\" :var \"is-lock\"}) #obj[`DBusMap`])"
  [{:keys [name] :as spec}]
  (let [get-var (spec->vars-extraction-fn spec)]
    #(let [v (cond
               (instance? DBusMap %)
               (when-let [val (.get ^DBusMap % name)]
                 (cond
                   (instance? Variant val) (.getValue ^Variant val)
                   :else
                   (do
                     (log/warn "A value in DBusMap is not Variant type - class:" (class val))
                     nil)))
               (map? %)
               (get % name))]
       (when (some? v) ; `false` is also valid value
         (get-var v)))))


(defn- spec->vars-extraction-fn
  "Generate a function that extract variables according to a given specification.

  Example:
   ((spec->vars-extraction-fn {:type :bool :val \"%{any}\" :var \"is-lock\"})
    \"some-value\")"
  [{:keys [type val var]}]
  (let [var-dict (if var
                   (partial hash-map var)
                   (constantly {}))]
    (cond
      (= :dict type)
      (comp (partial reduce
                     (fn [m res]
                       (if res
                         (merge m res)
                         (reduced nil)))
                     nil)
            (apply juxt (map dict-spec->var-extraction-fn val)))

      (= "%{any}" val)
      (with-meta var-dict {:val val})

      ;; TODO
      ;;  - candidates: %{0, 1, 2} or %{abc, efg, hij}
      ;;  - range: %{0 ~ 10}

      :else #(when (= val %) (var-dict %)))))


(defn- split-sig
  [sig]
  ;; NOTE
  ;;  it is not perfect.
  ;;  but i think it is good enough for my purpose.
  (re-seq #"[bynixdsv]|a(?:[bynixdsv]|\{(?:[bynixdsv][bynixdsv])+\})" sig))


(defn- count-specs
  [specs]
  (reduce #(let [next-spec (:next %2)]
             (cond-> (inc %1)
               next-spec
               (recur next-spec)))
          0 specs))


(defn- standardize-specs
  "Updates the position of specifications.
  If position of a specification is '%{continuous}', appends the specification to the previous specification using `:next` key.

  Example:
   (standardize-specs [{:type :str,  :val \"org.freedesktop.login1.Session\"}
                       {:type :dict, :val [{:type :bool, :name \"LockedHint\", :val \"%{any}\", :var \"is-lock\"}], :pos \"%{continuous}\"}])"
  [specs]
  (->> specs
       (map #(assoc % :sig (spec->sig %) :get-vars (spec->vars-extraction-fn %)))
       (reduce (fn [specs spec]
                 (if-not (= "%{continuous}" (:pos spec))
                   (conj specs spec)
                   (let [prev-spec (last specs)
                         spec (dissoc spec :pos)]
                     (if (int? (:pos prev-spec))
                       (conj specs           (assoc spec :pos (inc (:pos prev-spec))))
                       (conj (butlast specs) (assoc prev-spec :next spec))))))
               [])))


(defn vars-extraction-fn
  "Return a function that takes a value specification and returns a map that the specification contains :var.
  If DBus message is not matched to the specification, the function will return nil.

  Example:
   ((vars-extraction-fn [{:type :str,  :val \"org.freedesktop.login1.Session\"}
                         {:type :dict, :val [{:type :bool, :name \"LockedHint\", :val \"%{any}\", :var \"is-lock\"}], :pos \"%{continuous}\"}])
    (`signal` \"/org/freedesktop/login1/session/_32\"
            \"org.freedesktop.DBus.Properties\"
            \"PropertiesChanged\"
            [\"org.freedesktop.login1.Session\" {\"LockedHint\" true}]))"
  [specs]
  (let [num-of-specs (count-specs specs)
        specs (standardize-specs specs)
        max-pos (->> specs (map :pos) (filter int?) (reduce max 0))
        sigs->idxes-lst-fns (map #(partial sig->idxes-lst %) specs)]
    (log/debug {:spec specs})

    (fn [msg]
      (let [sigs (split-sig (msg-vals-sig msg))
            num-of-sigs (count sigs)
            idxes-lsts (and (< max-pos num-of-sigs)
                            (<= num-of-specs num-of-sigs)
                            (reduce (fn [idxes-lsts sigs->idxes-lst]
                                      (if-let [idxes-list (seq (sigs->idxes-lst sigs))]
                                        (conj idxes-lsts idxes-list)
                                        (reduced nil)))
                                    []
                                    sigs->idxes-lst-fns))]
        (when idxes-lsts
          (let [vals (msg-vals msg)
                specs (->> idxes-lsts
                           (map #(map first %)) ; no need rest indexes because indexes are always sequential (e.g. [4 5 6])
                           (map #(assoc %1 :idxes %2) specs)
                           ;; NOTE
                           ;;  imagin following case:
                           ;;   value X satisfy both spec A and B.
                           ;;   but, value Y satisfy only spec A.
                           ;;  for the above case, you have to check the speces with fewer candidates first.
                           (sort-by #(count (:idxes %))))

                {:keys [var-tbl]}
                (reduce (fn [{:keys [var-tbl idxes-used] :as res}
                             {:keys [pos get-vars idxes] :as head-spec}]
                          (when-not res
                            (reduced nil))

                          (if pos
                            (some->> (safe-nth vals pos)
                                     (get-vars)
                                     (update res :var-tbl merge))
                            (loop [{:keys [get-vars] :as spec} head-spec
                                   idx  (first idxes)
                                   idxes (rest idxes)
                                   var-tbl'    var-tbl
                                   idxes-used' idxes-used]
                              (when idx
                                (if-let [var-tbl' (and (not (contains? idxes-used' idx))
                                                       (some->> (safe-nth vals idx)
                                                                (get-vars)
                                                                (merge var-tbl')))]
                                  (if-let [next-spec (:next spec)]
                                    (recur next-spec (inc idx) idxes var-tbl' (conj idxes-used' idx))
                                    {:var-tbl    var-tbl'
                                     :idxes-used idxes-used'})
                                  (recur head-spec (first idxes) (rest idxes) var-tbl idxes-used))))))
                        {:var-tbl {}
                         :idxes-used #{}}
                        specs)]
            var-tbl))))))


(comment
  (method-call "/org/freedesktop/DBus" "org.freedesktop.DBus" "AddMatch" ["Type='signal'"])

  (let [msg (signal "/org/freedesktop/login1/session/_32"
                    "org.freedesktop.DBus.Properties"
                    "PropertiesChanged"
                    ["org.freedesktop.login1.Session" {"LockedHint" true}])
        get-vars (vars-extraction-fn [{:type :str,  :val "org.freedesktop.login1.Session"}
                                      {:type :dict, :val [{:type :bool, :name "LockedHint", :val "%{any}", :var "is-lock"}], :pos "%{continuous}"}])]
    (get-vars msg)))
