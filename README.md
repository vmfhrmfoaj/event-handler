# Event Handler

`event-handler` executes a bash script or program when an event(e.g. DBus) occurs for system automation.

## Installation

### Manual install

```bash
make install
```

## Configuration

You can monitor DBus messages using Wiresharek or `dubs-monitor` command:

```sh
dbus-monitor --system
dbus-monitor --session
```

And [D-Feet](https://wiki.gnome.org/Apps/DFeet) is helpful to inspect the format of DBus message.

### Example

This is configuration I am using.

```toml
[log]
  file = "/tmp/event-handler.log"  # (default: /dev/stdout)
  level = "info"                   # (default: warn)

[global]
  [global.handler]
    # If trigger was fired, handler will be executed after `delay` milliseconds.
    # You can override it in each 'trigger.handler' table.
    delay = 10 # (defualt: 10, minimum: 0, maximum: 3_600_000)

  [global.trigger]
    # Default value of minimal interval in milliseconds between the events,
    #  if an event is occurred within 200 milliseconds after the same type event has been occurred, new event will be ignored.
    # You can override it in each 'trigger' table.
    interval = 200 # (uint: msec, default: 200, minimum: 5 maximum: 1000)
    # If you have multiple triggers(i.e., have few 'trigger.dbus' tables in the same `trigger` table),
    #  `trigger_conjunction` decide how to conjunct sources in a trigger.
    # `trigger_conjunction` can be "and" or "or".
    # You can override it in each 'trigger' table.
    conjunction = "or" # (default: or)
    # A trigger can have multiple event sources to catch sequential events.
    # This is simair to how double-clicking works. To double-clicking, after the first click, you have to click again within a specific time.
    # `time_slice` is maximum interval from first event to last event of sequence events.
    time_slice = 100 # (uint: msec, default: 100, minimum: 5, maximum: 300)
    # If there is no order between events, `sequence_check` should set false.
    # `sequence_check` can be overrided in each trigger.
    sequence_check = false # (defualt: false)
    # By default, when an error occurred while a trigger is running, the trigger will try to recover up to 3 times.
    # If trigger can't recovery and `quit_on_error` is true, the program will be terminated.
    quit_on_error = true # (default: true)

[[handler]]
  name = "screen-lock-unlock"
  # If you want to use literal '%', you need to quote '%' with '%', like '%%'.
  # 'is-lock' should be defined in the `trigger` table.
  # below line is equal to `exec = ["bash", "-c", "echo $(if [ 'true' = %{is-lock} ]; then echo 'locked'; else echo 'unlocked'; fi)"]`
  exec = "echo $(if [ 'true' = %{is-lock} ]; then echo 'locked'; else echo 'unlocked'; fi)"

[[handler]]
  name = "wake-up"
  exec = ["/bin/echo", "program should be absolute path", "and it should be exists"]

[[handler]]
  name = "wake-up-2"
  exec = ["/bin/echo", "this is secondary action for wake-up trigger"]
  delay = 1_000


[[trigger]]
  name = "Screen (un)lock"
  interval = 1_000  # (uint: msec, default: 200, minimum: 5 maximum: 1000)

  # You can monitor DBus messages following commands on the terminal:
  #  - dbus-monitor --system
  #  - dbus-monitor --session
  [[trigger.dbus]]  # DBus source
    bus = "system"                                 # You can choose between 'system' and 'session'. (default: 'system')
    type = "signal"                                # You can choose in 'method-call', 'method-return', 'error', 'signal'.
    path = "/org/freedesktop/login1/session/**"    # You can use the glob pattern.
    interface = "org.freedesktop.DBus.Properties"
    member = "PropertiesChanged"

    # By default, `event-handler` doesn't check argument positions.
    # But, you can specify a position of an argument by setting `position`.
    [[trigger.dbus.argument]]
      type = "str"  # You can choose a type in 'byte', 'bool', 'int', 'double', 'str', 'dict'.
      value = "org.freedesktop.login1.Session"

    # If you don't set `position` automatically its `position` is next of previous argument.
    # In other words, there should be no argument in between the two arguments.
    [[trigger.dbus.argument]]
      type = "dict"  # If you select 'dict' type, you need to describe 'dict' table.
      # 0 indicates beginning of arguments, and -1 indicates end of arguments.
      # 'continuous' means a position is next of the above `argument` in the configuration.
      position = "%{continuous}"

      [[trigger.dbus.argument.dictionary]]
        type = "bool"
        name = "LockedHint"
        # '%{...}' is special form to express values.
        # You can use it to capture a value and pass it to the `handler`, or accept a range/group of numbers, so on.
        # Available forms are:
        #  - '%{any}':   any values
        #  - '%{0~10}':  0 to 10 (include 0 and 10)
        #  - '%{0,3,5}': 0, 3, 5
        # If you want to use literal '%', you need to quote '%' with '%', like '%%'.
        value = "%{any}"
        bind_to = "is-lock"  # Variable name of the value, it will be used by a handler.

  [trigger.handler]
    name = "screen-lock-unlock" # executor's name


[[trigger]]
  name = "Wake up"

  [[trigger.dbus]]
    bus = "system"
    type = "signal"
    path = "/org/freedesktop/login1/*"
    interface = "org.freedesktop.login1.Manager"
    member = "PrepareForSleep"

    [[trigger.dbus.argument]]
      type = "bool"
      value = false

  [[trigger.handler]]
    name = "wake-up"
    delay = 3_000  # wait 3 seconds before execute the handler

  [[trigger.handler]]
    name = "wake-up-2"
```

Please refer [example.config.toml](dev-resources/config.example.toml) for details.

# License

[MIT](https://opensource.org/licenses/MIT)
